# Armored Tetris

![Scheme](screenshot.jpg)

# Popis workspace

Celý workspace je součástí jedné solution v souboru HackathonApp\HackathonApp.sln

Po otevření je solution rozdělená na následující složky:
- **_Solution items** - obsahuje odkaz na tento popis
- **00_Common** - modul technických komponent a jeho testy
- **01_Business** - každý modul aplikace by měl mít vlastní složku, vzhledem k rozsahu je tato složka pouze jedna a má obecný název
- **Web** - webová aplikace a její testy

## Common module
Modul obsahuje následující technické komponenty
- **DataAccess** - Sada tříd zapouzdřující práci s databází prostřednictvím entity frameworku
- **Log** - Jednoduchá obálka zjednodušující používání log4net
- **ObjectFactory** - Sada tříd zjednodušujícíh registraci komponent do IoC containeru a jeho integraci do aplikace
- **GeneralSettings** - Třída umožňující přístup k app setting ve web.config

## Business
- **Tetris** - Implmenetace datových objektů, herní logiky a přístuk k herním statistikám

## Web
Jedná se o webovou aplikaci postavenou na standardní šabloně ASP.NET MVC. Tato šablona mimo komponent .NET frameworku používá bootstrap a jQuery a implementuje základní user management. Tato šablona byla upravena tak, aby místo modelu service locator implementovaný pomocí komponent Owin používala model dependency injection, který je implementovaný pomocí IoC kontainer Unity.

## Příprava workspace
Po stažení z git je třeba spustit build.bat


# Developer guidelines
- Používat SonarLint (součástí solution je přednastavená sada pravidel)
- Třídy reprezentující datové entity, repository pro přístup k datům, business služby apod. patří do projektu příslušného business module (v našem případě je to HackathonApp.Business)
- Do webového projektu patří pouze volání business služeb a logika nezbytná k zobrazení dat.
- Datová entita implementuje rozhraní IPersistantObject nebo IDataObject a obsahuje pouze jednoduché property pro přístup k datům. Business logiku by neměly obsahovat vůbec nebo jen v omezených případech.
- Datové entity je třeba zaregistrovat do DataContextu prostřednictvím implementace rozhraní IDataContextModule (jedna pro každý business modul)
- Pro implementaci datových entit existuje základní implementace DataObjectBase 
- Všechny CRUD oprace se provádějí v repository (třída se jménem XyzRepository, která implementuje rozhraní IRepository nebo IEntityRepository a případně dědí z RepositoryBase nebo EntityRepositoryBase)
- Základní implementace repository jsou registrované v IoC a je možné je použít přímo. Pokud nestačí generická implementace, je třeba k repository vytvořit rozhraní a zaregistrovat pomocí atributu HackathonApp.Common.ObjectFactory.Component
- Business služby jsou třídy implementují vlastní rozhraní a jsou zaregistrované v IoC pomocí atributu Component.
- Na metodách služeb je možné používat následující atributy. Aby atributy fungovaly je nutné při registraci služby nastavit příznak UseProxy na true.
    - LogMethod - zaloguje sluštění a ukončení metody a v případě potřeby i výjimku, která v metodě vznikla. Logování výsledku a parametrů metody je volitelné a je třeba brát v úvahu, že do logu se nesmí dostat citlivá data.

- log je vhodné používat prostřednictvím následující proměnné:
```
private readonly ILogger log = new LoggerBase(nameof(MyClass));
```
- IoC kontejner by neměl být používán přímo, ale prostřednictvím modelu constructor injection
- Registrace komponenty má následující atributy:
    - ImplementationOf - typ rozhraní, je povinné
    - Name - používá se při pomenování více implementací stejného rozhraní
    - UseProxy - zapnutím se zpřístupní používání některých atributů na úrovni metod, generuje dynamickou proxy kolem registrované komponenty
    - Lifetime - slouží k řízení životního cyklu instancí (Transient, PerRequest, Singleton)

- Unit testy se tvoří pomocí frameworku NUnit.
- V implementaci testu je možné využít FluentAssertions a NSubstitute
- Unit testy nesmí využívat databázi (její služby je třeba mockovat)




