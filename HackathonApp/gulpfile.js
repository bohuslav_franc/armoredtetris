var gulp = require('gulp'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    os = require('os'),
    browserSync = require('browser-sync').create(),
    karma = require('karma');

var projectUrl = "http://localhost:51398/";
if (os.hostname() == "NB2090") { //B. Franc
    projectUrl = "http://localhost/HackAthon/";
}

// Compile main.less with source maps    
gulp.task('less', function () {
    return gulp.src('HackathonApp/Content/main.less')
        .pipe(sourcemaps.init())
        .pipe(less({ relativeUrls: true }))
        .on('error', logError)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('HackathonApp/Content'));
});

// Watch changes and compile less
gulp.task('watching:less', function (done) {
    gulp.watch(['HackathonApp/Content/**/*.less'], ['less']);
});

// Open project on port 3000 and listen css and js changes
gulp.task('livesync', function () {
    console.log('Loading browser-sync ...');
    browserSync.init({
        files: ["HackathonApp/**/*.cshtml", "HackathonApp/Content/main.css", 'HackathonApp/ClientApp/**/*.js'],
        proxy: projectUrl,
        reloadDelay: 500,
        open: false // VS works as admin, can not open new browser tab in existing browser instance
    });
});


/**
 * Run test once and exit
 */

gulp.task('karma', function (done) {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('watching:karma', function (done) {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js'
    }, done).start();
});

// Logs error and continues processing
function logError(error) {
    console.log(error.toString());
    this.emit('end');
}