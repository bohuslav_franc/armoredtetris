﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackathonApp.Business
{
    /// <summary>
    /// Controls top level game logic
    /// </summary>
    public interface IGameManager
    {
        /// <summary>
        /// New user was connected and waiting to play a game
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="identity"></param>
        void UserConnected(string clientId, string identity);

        /// <summary>
        /// User was disconnected from game
        /// </summary>
        /// <param name="clientId"></param>
        void UserDisconnected(string clientId);

        /// <summary>
        /// User confirmed, he is ready to play a gmae
        /// </summary>
        /// <param name="clientId"></param>
        void UserIsReady(string clientId);

        /// <summary>
        /// User press a key which should be handled by game logic
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="key"></param>
        void KeyPressed(string clientId, string key);

    }
}
