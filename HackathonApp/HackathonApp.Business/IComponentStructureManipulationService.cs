﻿using System;

namespace HackathonApp.Business
{
    public enum ComponentType
    {
        I = 0,
        J = 1,
        O = 2,
        S = 3,
        T = 4,
        L = 5,
        Z = 6
    }

    /// <summary>
    /// Service for creating and manipulating game shape structure
    /// </summary>
    public interface IComponentStructureManipulationService
    {
        /// <summary>
        /// Initializes shape cache
        /// </summary>
        void PrepareCommonStructures();

        /// <summary>
        /// Returns structure of specified shape type and rotation
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rotaition"></param>
        /// <returns></returns>
        ComponentStructure GetStructure(ComponentType componentId, int rotaition);

        /// <summary>
        /// Get random shape type and random rotation
        /// </summary>
        /// <returns></returns>
        Tuple<ComponentType, int> GetRandomShape();

    }
}
