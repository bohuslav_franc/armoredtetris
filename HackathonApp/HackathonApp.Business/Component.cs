﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Security.Cryptography.X509Certificates;
using System.Timers;

namespace HackathonApp.Business
{
    public struct Point
    {
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public Point AddX(int i)
        {
            return new Point(X + i, Y);
        }
        public Point AddY(int i)
        {
            return new Point(X, Y + i);
        }
    }



    public class ComponentStructure
    {
        public ComponentStructure(params Point[] points)
        {
            this.Points = points;
        }

        public Point[] Points { get; set; }
    }

    public class FallingComponent
    {
        public ComponentStructure Structure { get; set; }
        public ComponentType StructureId { get; set; }
        public int Rotation { get; set; }
        public Point Point { get; set; }
    }

    public class GameMatrix
    {
        public const int Width = 11;
        public const int Height = 22;


        public GameMatrix()
        {
            Cells = new bool[Width,Height];
        }

        /// <summary>
        /// Columns, rows
        /// </summary>
        public bool[,] Cells { get; set; }
    }

    public class Player
    {
        public string ClientId { get; set; }
        public string NickName { get; set; }
        public bool IsReady { get; set; }
    }

    public class GameBoard
    {
        public GameBoard()
        {
            NextComponents = new List<Tuple<ComponentType, int>>();
        }

        public Game Game { get; set; }

        public Player Player { get; set; }

        public GameMatrix Matrix { get; set; }

        public FallingComponent FallingComponent { get; set; }

        public IList<Tuple<ComponentType, int>> NextComponents { get; set; }

        public GameBoard OpponentsBoard { get; set; }

        public bool CooldownIsRunning { get; set; }

        public int CoolDownLeft { get; set; }

        public int RowsToBeAdded { get; set; }

    }

    public enum GameState
    {
        WaitingOponent, WaitingPlayersAreReady, Playing, Stopped
    }

    public enum GameWinner
    {
        None, Player1, Player2
    }

    public class Game
    {
        public GameState State { get; set; }

        public GameWinner Winner { get; set; }

        public Timer Timer { get; set; }

        public GameBoard Board1 { get; set; }
        public GameBoard Board2 { get; set; }

    }
}
