﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackathonApp.Business
{

    public enum MoveDirection
    {
        Left, Right, Down
    }

    public interface IBoardManager
    {
        void Start(GameBoard board);

        void Move(GameBoard board);

        void Rotate(GameBoard board);

        void MoveFalingComponent(GameBoard board, MoveDirection direction);

        bool IsBoardFull(GameBoard board);

        void GenerateNextComponents(GameBoard board);
    }
}
