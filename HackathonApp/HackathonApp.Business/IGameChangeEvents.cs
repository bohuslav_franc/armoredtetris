﻿namespace HackathonApp.Business
{
    /// <summary>
    /// Set of events originated from server side
    /// </summary>
    public interface IGameChangeEvents
    {
        /// <summary>
        /// Game matrix was changed and it needs to be redrawn
        /// </summary>
        /// <param name="board"></param>
        /// <param name="matrix"></param>
        void matrixChanged(GameBoard board, GameMatrix matrix);

        /// <summary>
        /// Falling component moved and it needs to be redrawn
        /// </summary>
        /// <param name="board"></param>
        /// <param name="component"></param>
        void componentMoved(GameBoard board, FallingComponent component);

        /// <summary>
        /// Next shapes was changed and thay need to be redrawn
        /// </summary>
        /// <param name="board"></param>
        /// <param name="component"></param>
        void setNextComponents(GameBoard board, ComponentStructure[] component);

        /// <summary>
        /// Player destroys a row and cooldown started for oponent
        /// </summary>
        /// <param name="board"></param>
        /// <param name="duration"></param>
        void cooldownStarted(GameBoard board, int duration);

        /// <summary>
        /// Cooldown has expired
        /// </summary>
        /// <param name="board"></param>
        void cooldownExpired(GameBoard board);

        /// <summary>
        /// Full row was destroyed
        /// </summary>
        /// <param name="board"></param>
        void rowDestroyed(GameBoard board);

        /// <summary>
        /// Cooldown expires and new junk was generated for a player
        /// </summary>
        /// <param name="board"></param>
        void junkCreated(GameBoard board);

    }
}