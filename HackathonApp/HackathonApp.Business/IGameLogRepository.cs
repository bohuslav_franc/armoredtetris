﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackathonApp.Common.DataAccess;

namespace HackathonApp.Business
{
    /// <summary>
    /// Database repository of log of played games
    /// </summary>
    public interface IGameLogRepository : IRepository<GameLog>
    {
        /// <summary>
        /// Returns all game played by specific user
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        IEnumerable<GameLog> GetMyHistory(string userName);
    }

    /// <summary>
    /// Database repository of player ratings
    /// </summary>
    public interface IPlayerRatingRepository : IRepository<PlayerRating>
    {
        /// <summary>
        /// Returns 10 players with highest rating
        /// </summary>
        /// <returns></returns>
        IEnumerable<PlayerRating> GetTopTen();

        /// <summary>
        /// Return rating for specified player
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        PlayerRating GetByPlayer(string player);
    }
}
