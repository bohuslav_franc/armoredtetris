﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using HackathonApp.Common.DataAccess;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business
{
    [Component(ImplementationOf = typeof(IDataContextModule), Name = "Business entities data context module")]
    public class BusinessEntities : IDataContextModule
    {
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GameLog>();
            modelBuilder.Entity<PlayerRating>();
        }
    }

    [Component(ImplementationOf = typeof(IInitialDataCreator), Name = "Business entities initial data.")]
    public class BusinessEntitiesInitialData : IInitialDataCreator
    {
        public void InitializeData(IDataContext context)
        {
            var random = new Random();

            for (int i = 0; i < 20; i++)
            {
                context.Create(new PlayerRating()
                {
                    PlayerName = "Player" + (i + 1),
                    Rating = random.Next(500, 1100)
                });
            }

            for (int p1 = 0; p1 < 20; p1++)
            {
                for (int p2 = 0; p2 < 20; p2++)
                {
                    var result = GetRandomResult(random);

                    context.Create(new GameLog()
                    {
                        GameEnded = DateTime.Now,
                        Player1 = "Player" + (p1 + 1),
                        Player2 = "Player" + (p2 + 1),
                        Player1Result = result,
                        Player2Result = 1 - result
                    });
                }
            }

        }

        private double GetRandomResult(Random random)
        {
            int r = random.Next(0, 3);

            if (r == 2)
            {
                return 0.5;
            }

            return r;
        }
    }

}
