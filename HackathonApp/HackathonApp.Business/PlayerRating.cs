﻿using HackathonApp.Common.DataAccess;

namespace HackathonApp.Business
{
    /// <summary>
    /// Represents data object for storing player rating do DB
    /// </summary>
    public class PlayerRating : DataObjectBase
    {
        /// <summary>
        /// Gets or sets the player login
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the players current rating
        /// </summary>
        public double Rating { get; set; }
    }
}