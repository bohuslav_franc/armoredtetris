﻿namespace HackathonApp.Business
{
    public interface IRatingCalculator
    {
        /// <summary>
        /// Returns predefined initial rating for new players
        /// </summary>
        /// <returns></returns>
        double GetInitialRating();

        /// <summary>
        /// Computes new rating for the player
        /// </summary>
        /// <param name="currentRating"></param>
        /// <param name="opponentRating"></param>
        /// <param name="actualGameResult"></param>
        /// <returns></returns>
        double NewRating(double currentRating, double opponentRating, double actualGameResult);

    }
}
