﻿using System;
using HackathonApp.Common.DataAccess;

namespace HackathonApp.Business
{
    public class GameLog : DataObjectBase
    {
        public DateTime GameEnded { get; set; }

        public string Player1 { get; set; }

        public string Player2 { get; set; }

        public double Player1Result { get; set; }

        public double Player2Result { get; set; }

    }
}