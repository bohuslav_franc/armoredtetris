﻿using System;
using System.Collections.Generic;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IComponentStructureManipulationService), Lifetime = ComponentLifetime.Singleton)]
    public class ComponentStructureManipulationService : IComponentStructureManipulationService
    {
        private static IDictionary<int, ComponentStructure> structures = new Dictionary<int, ComponentStructure>();
        private static Random random = new Random();

        private Point[] BasicI = new[] { new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0) };

        private Point[] BasicJ = new[] { new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(1, 1) };
        private Point[] BasicJ1 = new[] { new Point(0, -1), new Point(0, 0), new Point(0, 1), new Point(-1, 1) };
        private Point[] BasicJ2 = new[] { new Point(-1, -1), new Point(-1, 0), new Point(0, 0), new Point(1, 0) };
        private Point[] BasicJ3 = new[] { new Point(1, -1), new Point(0, -1), new Point(0, 0), new Point(0, 1) };

        private Point[] BasicO = new[] { new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) };

        private Point[] BasicS = new[] { new Point(0, 0), new Point(1, 0), new Point(-1, 1), new Point(0, 1) };
        private Point[] BasicS1 = new[] { new Point(-1, -1), new Point(-1, 0), new Point(0, 0), new Point(0, 1) };

        private Point[] BasicT0 = new[] { new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(0, 1) };
        private Point[] BasicT1 = new[] { new Point(0, -1), new Point(-1, 0), new Point(0, 0), new Point(0, 1) };
        private Point[] BasicT2 = new[] { new Point(-1, 0), new Point(0, 0), new Point(0, -1), new Point(1, 0) };
        private Point[] BasicT3 = new[] { new Point(0, -1), new Point(0, 0), new Point(1, 0), new Point(0, 1) };

        private Point[] BasicL0 = new[] { new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(-1, 1) };
        private Point[] BasicL1 = new[] { new Point(-1, -1), new Point(0, -1), new Point(0, 0), new Point(0, 1) };
        private Point[] BasicL2 = new[] { new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(1, -1) };
        private Point[] BasicL3 = new[] { new Point(0, -1), new Point(0, 0), new Point(0, 1), new Point(1, 1) };

        private Point[] BasicZ0 = new[] { new Point(-1, 0), new Point(0, 0), new Point(0, 1), new Point(1, 1) };
        private Point[] BasicZ1 = new[] { new Point(0, -1), new Point(0, 0), new Point(-1, 0), new Point(-1, 1) };

        public ComponentStructureManipulationService()
        {
            PrepareCommonStructures();
        }


        public void PrepareCommonStructures()
        {
            string colorI = "red";
            string colorJ = "red";
            string colorO = "red";
            string colorS = "red";
            string colorT = "red";
            string colorL = "red";
            string colorZ = "red";

            structures[GetStructureId(ComponentType.I, 0)] = new ComponentStructure(BasicI);
            structures[GetStructureId(ComponentType.I, 1)] = new ComponentStructure(SwapCoordinates(BasicI));
            structures[GetStructureId(ComponentType.I, 2)] = new ComponentStructure(BasicI);
            structures[GetStructureId(ComponentType.I, 3)] = new ComponentStructure(SwapCoordinates(BasicI));

            structures[GetStructureId(ComponentType.J, 0)] = new ComponentStructure(BasicJ);
            structures[GetStructureId(ComponentType.J, 1)] = new ComponentStructure(BasicJ1);
            structures[GetStructureId(ComponentType.J, 2)] = new ComponentStructure(BasicJ2);
            structures[GetStructureId(ComponentType.J, 3)] = new ComponentStructure(BasicJ3);

            structures[GetStructureId(ComponentType.O, 0)] = new ComponentStructure(BasicO);
            structures[GetStructureId(ComponentType.O, 1)] = new ComponentStructure(BasicO);
            structures[GetStructureId(ComponentType.O, 2)] = new ComponentStructure(BasicO);
            structures[GetStructureId(ComponentType.O, 3)] = new ComponentStructure(BasicO);

            structures[GetStructureId(ComponentType.S, 0)] = new ComponentStructure(BasicS);
            structures[GetStructureId(ComponentType.S, 1)] = new ComponentStructure(BasicS1);
            structures[GetStructureId(ComponentType.S, 2)] = new ComponentStructure(BasicS);
            structures[GetStructureId(ComponentType.S, 3)] = new ComponentStructure(BasicS1);

            structures[GetStructureId(ComponentType.T, 0)] = new ComponentStructure(BasicT0);
            structures[GetStructureId(ComponentType.T, 1)] = new ComponentStructure(BasicT1);
            structures[GetStructureId(ComponentType.T, 2)] = new ComponentStructure(BasicT2);
            structures[GetStructureId(ComponentType.T, 3)] = new ComponentStructure(BasicT3);

            structures[GetStructureId(ComponentType.L, 0)] = new ComponentStructure(BasicL0);
            structures[GetStructureId(ComponentType.L, 1)] = new ComponentStructure(BasicL1);
            structures[GetStructureId(ComponentType.L, 2)] = new ComponentStructure(BasicL2);
            structures[GetStructureId(ComponentType.L, 3)] = new ComponentStructure(BasicL3);

            structures[GetStructureId(ComponentType.Z, 0)] = new ComponentStructure(BasicZ0);
            structures[GetStructureId(ComponentType.Z, 1)] = new ComponentStructure(BasicZ1);
            structures[GetStructureId(ComponentType.Z, 2)] = new ComponentStructure(BasicZ0);
            structures[GetStructureId(ComponentType.Z, 3)] = new ComponentStructure(BasicZ1);

        }


        public ComponentStructure GetStructure(ComponentType componentId, int rotaition)
        {
            int index = GetStructureId(componentId, rotaition);
            return structures[index];
        }

        public Tuple<ComponentType, int> GetRandomShape()
        {
            int type = random.Next(0, 7);
            int rotation = random.Next(0, 3);

            return new Tuple<ComponentType, int>((ComponentType)type, rotation);
        }


        #region private menmbers 

        private int GetStructureId(ComponentType componentId, int rotaition)
        {
            return ((int)componentId) * 4 + rotaition;
        }

        private Point[] SwapCoordinates(Point[] points)
        {
            Point[] result = new Point[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                result[i] = new Point(points[i].Y, points[i].X);
            }
            return result;
        }

        #endregion
    }
}
