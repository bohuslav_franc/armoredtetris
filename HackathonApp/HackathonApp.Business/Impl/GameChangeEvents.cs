﻿using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IGameChangeEvents))]
    public class GameChangeEvents : IGameChangeEvents
    {
        private readonly IGameChangeClientEvents gameChangeClientEvents;

        public GameChangeEvents(IGameChangeClientEvents gameChangeClientEvents)
        {
            this.gameChangeClientEvents = gameChangeClientEvents;
        }


        public void matrixChanged(GameBoard board, GameMatrix matrix)
        {
            gameChangeClientEvents.matrixChanged(board.Player, matrix);
            gameChangeClientEvents.opponentMatrixChanged(board.OpponentsBoard.Player, matrix);
        }

        public void componentMoved(GameBoard board, FallingComponent component)
        {
            gameChangeClientEvents.componentMoved(board.Player, component);
            gameChangeClientEvents.opponentComponentMoved(board.OpponentsBoard.Player, component);
        }

        public void setNextComponents(GameBoard board, ComponentStructure[] component)
        {
            gameChangeClientEvents.setNextComponents(board.Player, component);
        }

        public void cooldownStarted(GameBoard board, int duration)
        {
            gameChangeClientEvents.cooldownStarted(board.Player, duration);
            gameChangeClientEvents.opponentCooldownStarted(board.OpponentsBoard.Player, duration);
        }

        public void cooldownExpired(GameBoard board)
        {
            gameChangeClientEvents.cooldownExpired(board.Player);
            gameChangeClientEvents.opponentCooldownExpired(board.OpponentsBoard.Player);
        }

        public void rowDestroyed(GameBoard board)
        {
            gameChangeClientEvents.rowDestroyed(board.Player);
            gameChangeClientEvents.rowDestroyed(board.OpponentsBoard.Player);
        }

        public void junkCreated(GameBoard board)
        {
            gameChangeClientEvents.junkCreated(board.Player);
            gameChangeClientEvents.junkCreated(board.OpponentsBoard.Player);
        }

    }
}
