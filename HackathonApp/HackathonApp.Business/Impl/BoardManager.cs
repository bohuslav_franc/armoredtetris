﻿using System;
using System.Collections.Generic;
using System.Linq;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IBoardManager))]
    public class BoardManager : IBoardManager
    {
        private readonly IGameChangeEvents gameChangeEvents;
        private readonly IComponentStructureManipulationService componentStructure;

        public BoardManager(IGameChangeEvents gameChangeEvents, IComponentStructureManipulationService componentStructure)
        {
            this.gameChangeEvents = gameChangeEvents;
            this.componentStructure = componentStructure;
        }

        private const int CoolDownDuration = 10;

        /// <summary>
        /// Prepares board for game start
        /// </summary>
        /// <param name="board"></param>
        public void Start(GameBoard board)
        {
            board.Matrix = new GameMatrix();

            NewFallingComponent(board);
        }

        /// <summary>
        /// Make one game tick
        /// </summary>
        /// <param name="board"></param>
        public void Move(GameBoard board)
        {
            // one component just fell to the bottom, create another one
            if (board.FallingComponent == null)
            {
                NewFallingComponent(board);
            }
            else
            {
                CheckCooldown(board);
                MoveFalingComponent(board, MoveDirection.Down);
            }
        }

        /// <summary>
        /// Cycles 4 basic rotation angles, rotation cannot be done wher ther will be a collision in new position
        /// </summary>
        /// <param name="board"></param>
        public void Rotate(GameBoard board)
        {
            if (board.FallingComponent == null)
            {
                return;
            }

            var rotation = board.FallingComponent.Rotation;
            rotation--;
            if (rotation < 0)
            {
                rotation = 3;
            }
            var newStructure = componentStructure.GetStructure(board.FallingComponent.StructureId, rotation);

            if (!IsColision(board.Matrix, newStructure, board.FallingComponent.Point))
            {
                board.FallingComponent.Rotation = rotation;
                board.FallingComponent.Structure = newStructure;
                gameChangeEvents.componentMoved(board, board.FallingComponent);
            }
        }

        /// <summary>
        /// Moves falling component in specific direction
        /// </summary>
        /// <param name="board"></param>
        /// <param name="direction"></param>
        public void MoveFalingComponent(GameBoard board, MoveDirection direction)
        {
            if (board.FallingComponent == null)
            {
                return;
            }

            var newPoint = board.FallingComponent.Point;
            switch (direction)
            {
                case MoveDirection.Left:
                    newPoint = newPoint.AddX(-1);
                    break;
                case MoveDirection.Right:
                    newPoint = newPoint.AddX(1);
                    break;
                case MoveDirection.Down:
                    newPoint = newPoint.AddY(-1);
                    break;
            }

            // check for collision to make sure, move is valid
            if (!IsColision(board.Matrix, board.FallingComponent.Structure, newPoint))
            {
                board.FallingComponent.Point = newPoint;
                gameChangeEvents.componentMoved(board, board.FallingComponent);
            }
            else
            {
                if (direction == MoveDirection.Down)
                {
                    SetFallingComponentToMatrix(board.Matrix, board.FallingComponent);
                    int rowsRemoved = RemoveFullRows(board.Matrix);
                    board.FallingComponent = null;
                    gameChangeEvents.matrixChanged(board, board.Matrix);
                    gameChangeEvents.componentMoved(board, null);

                    //some rows was removed, start cooldown
                    if (rowsRemoved > 0)
                    {
                        gameChangeEvents.rowDestroyed(board);

                        if (board.CooldownIsRunning)
                        {
                            ResetCooldown(board);
                        }
                        else
                        {
                            StartCooldown(board, rowsRemoved);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Return true is last row of board has any filled cell
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public bool IsBoardFull(GameBoard board)
        {
            return HasRowAny(board.Matrix, GameMatrix.Height - 1);
        }

        /// <summary>
        /// Copy falling component structure to game matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="fallingComponent"></param>
        private void SetFallingComponentToMatrix(GameMatrix matrix, FallingComponent fallingComponent)
        {
            foreach (var structurePoint in fallingComponent.Structure.Points)
            {
                int colisionX = structurePoint.X + fallingComponent.Point.X;
                int colisionY = structurePoint.Y + fallingComponent.Point.Y;

                if (colisionY < GameMatrix.Height)
                {
                    matrix.Cells[colisionX, colisionY] = true;
                }
            }
        }

        /// <summary>
        /// Get next falling component from queue
        /// </summary>
        /// <param name="board"></param>
        private void NewFallingComponent(GameBoard board)
        {
            var next = board.NextComponents.First();
            board.NextComponents = board.NextComponents.Skip(1).ToList();
            var nextType = next.Item1;
            var rotation = next.Item2;

            GenerateNextComponents(board);

            board.FallingComponent = new FallingComponent()
            {
                Point = new Point(5, 21),
                Rotation = rotation,
                StructureId = nextType,
                Structure = componentStructure.GetStructure(nextType, rotation)
            };
            gameChangeEvents.componentMoved(board, board.FallingComponent);

            var nextStructures = board.NextComponents.Take(2)
                .Select(i => componentStructure.GetStructure(i.Item1, i.Item2))
                .ToArray();

            gameChangeEvents.setNextComponents(board, nextStructures);
        }

        /// <summary>
        /// Adds several new components to queue when necessary
        /// </summary>
        /// <param name="board"></param>
        public void GenerateNextComponents(GameBoard board)
        {
            if (board.NextComponents.Count > 3)
            {
                return;
            }

            for (int i = 0; i < 3; i++)
            {
                var nextItem = componentStructure.GetRandomShape();

                board.NextComponents.Add(nextItem);
                board.OpponentsBoard.NextComponents.Add(nextItem);
            }
        }



        private bool IsColision(GameMatrix matrix, ComponentStructure structure, Point point)
        {
            foreach (var structurePoint in structure.Points)
            {
                int colisionX = structurePoint.X + point.X;
                int colisionY = structurePoint.Y + point.Y;

                if (colisionY < 0)
                {
                    return true;
                }

                if (colisionY < GameMatrix.Height)
                {
                    if (colisionX < 0 || colisionX >= GameMatrix.Width)
                    {
                        return true;
                    }

                    if (matrix.Cells[colisionX, colisionY])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private int RemoveFullRows(GameMatrix boardMatrix)
        {
            int rowsRemoved = 0;
            for (int y = GameMatrix.Height - 1; y >= 0; y--)
            {
                if (IsRowFull(boardMatrix, y))
                {
                    rowsRemoved++;
                    RemoveRow(boardMatrix, y);
                }
            }
            return rowsRemoved;
        }

        private void RemoveRow(GameMatrix boardMatrix, int row)
        {
            for (int y = row; y < GameMatrix.Height - 1; y++)
            {
                MoveRow(boardMatrix, y + 1, y);
            }
        }

        private void MoveRow(GameMatrix matrix, int from, int to)
        {
            for (int x = 0; x < GameMatrix.Width; x++)
            {
                matrix.Cells[x, to] = matrix.Cells[x, from];
            }
        }

        private bool IsRowFull(GameMatrix matrix, int row)
        {
            for (int x = 0; x < GameMatrix.Width; x++)
            {
                if (!matrix.Cells[x, row])
                {
                    return false;
                }
            }
            return true;
        }

        private bool HasRowAny(GameMatrix matrix, int row)
        {
            for (int x = 0; x < GameMatrix.Width; x++)
            {
                if (matrix.Cells[x, row])
                {
                    return true;
                }
            }
            return false;
        }

        private void StartCooldown(GameBoard board, int rowsRemoved)
        {
            var oponentBoard = board.OpponentsBoard;

            if (oponentBoard.CooldownIsRunning)
            {
                oponentBoard.RowsToBeAdded += rowsRemoved;
            }
            else
            {
                oponentBoard.CoolDownLeft = CoolDownDuration;
                oponentBoard.CooldownIsRunning = true;
                oponentBoard.RowsToBeAdded = rowsRemoved;

                // game cycle takes 500 miliseconds
                gameChangeEvents.cooldownStarted(board.OpponentsBoard, CoolDownDuration * 500);
            }

        }

        private void ResetCooldown(GameBoard board)
        {
            if (board.CooldownIsRunning)
            {
                board.CoolDownLeft = 0;
                board.CooldownIsRunning = false;
                board.RowsToBeAdded = 0;
                gameChangeEvents.cooldownExpired(board);
            }
        }

        private void CheckCooldown(GameBoard board)
        {
            if (board.CooldownIsRunning)
            {
                if (board.CoolDownLeft <= 0)
                {
                    CreateJunk(board.Matrix, board.RowsToBeAdded);
                    gameChangeEvents.junkCreated(board);
                    ResetCooldown(board);

                    gameChangeEvents.matrixChanged(board, board.Matrix);
                }

                board.CoolDownLeft--;
            }
        }

        private void CreateJunk(GameMatrix matrix, int rows)
        {
            for (int y = GameMatrix.Width - 1; y >= rows; y--)
            {
                MoveRow(matrix, y - rows, y);
            }

            var rnd = new Random();

            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < GameMatrix.Width; x++)
                {
                    matrix.Cells[x, y] = GetRandomCell(rnd); 
                }
            }
        }

        private bool GetRandomCell(Random random)
        {
            return (random.Next(100) > 60);
        }

    }
}
