﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackathonApp.Common.DataAccess;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IGameLogRepository))]
    public class GameLogRepository : RepositoryBase<GameLog>, IGameLogRepository
    {
        public GameLogRepository(IDataContext context) : base(context)
        {
        }

        public IEnumerable<GameLog> GetMyHistory(string userName)
        {
            return Entity
                .Where(g => g.Player1 == userName || g.Player2 == userName)
                .OrderByDescending(g => g.GameEnded)
                .ToList();
        }
    }


    [Component(ImplementationOf = typeof(IPlayerRatingRepository))]
    public class PlayerRatingRepository : RepositoryBase<PlayerRating>, IPlayerRatingRepository
    {
        public PlayerRatingRepository(IDataContext context) : base(context)
        {
        }

        public IEnumerable<PlayerRating> GetTopTen()
        {
            return Entity
                .OrderByDescending(p => p.Rating)
                .Take(10)
                .ToList();
        }

        public PlayerRating GetByPlayer(string player)
        {
            return Entity.FirstOrDefault(rating => rating.PlayerName == player);
        }
    }


}

