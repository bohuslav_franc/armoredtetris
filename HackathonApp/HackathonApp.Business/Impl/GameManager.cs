﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using HackathonApp.Common;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IGameManager), Lifetime = ComponentLifetime.Singleton)]
    public class GameManager : IGameManager
    {
        private readonly IList<Game> currentGames = new List<Game>();

        private readonly IGameChangeClientEvents gameEvents;
        private readonly IBoardManager boardManager;
        private readonly IGameLogRepository gameLogRepo;
        private readonly IPlayerRatingRepository playerRatingRepo;
        private readonly IRatingCalculator ratingCalculator;

        public GameManager(IGameChangeClientEvents gameEvents, IBoardManager boardManager,
            IGameLogRepository gameLogRepo, IPlayerRatingRepository playerRatingRepo, IRatingCalculator ratingCalculator)
        {
            this.gameEvents = gameEvents;
            this.boardManager = boardManager;
            this.gameLogRepo = gameLogRepo;
            this.playerRatingRepo = playerRatingRepo;
            this.ratingCalculator = ratingCalculator;
        }


        public void UserConnected(string clientId, string identity)
        {
            var waitingGame = currentGames.FirstOrDefault(g => g.State == GameState.WaitingOponent);

            if (waitingGame == null)
            {
                CreateNewWaitingGame(clientId, identity);
            }
            else
            {
                JoinPlayers(waitingGame, clientId, identity);
            }
        }

        public void UserDisconnected(string clientId)
        {
            var board = GetClientsBoard(clientId);

            if (board != null && board.Game.State == GameState.Playing)
            {
                board.Matrix.Cells[0, GameMatrix.Height - 1] = true;

                bool board1Full = boardManager.IsBoardFull(board.Game.Board1);
                bool board2Full = boardManager.IsBoardFull(board.Game.Board2);

                StopGame(board.Game, board1Full, board2Full);
            }
        }

        public void UserIsReady(string clientId)
        {
            var board = GetClientsBoard(clientId);
            lock (board.Game)
            {
                board.Player.IsReady = true;

                gameEvents.opponentIsReady(board.OpponentsBoard.Player);

                if (board.OpponentsBoard.Player.IsReady)
                {
                    StartGame(board.Game);
                }
            }
        }

        public void KeyPressed(string clientId, string key)
        {
            var board = GetClientsBoard(clientId);

            lock (board.Game)
            {
                if (key == "L")
                {
                    boardManager.MoveFalingComponent(board, MoveDirection.Left);
                }
                if (key == "R")
                {
                    boardManager.MoveFalingComponent(board, MoveDirection.Right);
                }
                if (key == "D")
                {
                    boardManager.MoveFalingComponent(board, MoveDirection.Down);
                }
                if (key == "U")
                {
                    boardManager.Rotate(board);
                }
            }
        }

        #region private members

        private void CreateNewWaitingGame(string clientId, string userNickName)
        {
            var game = new Game()
            {
                State = GameState.WaitingOponent,
                Board1 = new GameBoard()
                {
                    Player = new Player()
                    {
                        ClientId = clientId,
                        NickName = userNickName
                    }
                }
            };
            game.Board1.Game = game;

            currentGames.Add(game);
        }

        private void JoinPlayers(Game game, string clientId, string userNickName)
        {
            lock (game)
            {
                game.Board2 = new GameBoard()
                {
                    Game = game,
                    Player = new Player()
                    {
                        ClientId = clientId,
                        NickName = userNickName
                    }
                };
                game.State = GameState.WaitingPlayersAreReady;

                game.Board1.OpponentsBoard = game.Board2;
                game.Board2.OpponentsBoard = game.Board1;

                gameEvents.opponentConnected(game.Board1.Player, game.Board2.Player);
                gameEvents.opponentConnected(game.Board2.Player, game.Board1.Player);
            }
        }

        private GameBoard GetClientsBoard(string clientId)
        {
            foreach (Game game in currentGames)
            {
                if (game.Board1 != null && game.Board1.Player.ClientId == clientId)
                {
                    return game.Board1;
                }
                if (game.Board2 != null && game.Board2.Player.ClientId == clientId)
                {
                    return game.Board2;
                }
            }

            return null;
        }

        private void StartGame(Game game)
        {
            boardManager.GenerateNextComponents(game.Board1);

            boardManager.Start(game.Board1);
            boardManager.Start(game.Board2);

            game.Timer = new Timer(500);
            game.Timer.AutoReset = true;
            game.Timer.Elapsed += (sender, args) =>
            {
                GameTick(game);
            };
            game.Timer.Start();

            game.State = GameState.Playing;
        }

        private void GameTick(Game game)
        {

            if (game.State == GameState.Playing)
            {
                lock (game)
                {
                    bool board1Full = boardManager.IsBoardFull(game.Board1);
                    bool board2Full = boardManager.IsBoardFull(game.Board2);

                    if (board1Full || board2Full)
                    {
                        StopGame(game, board1Full, board2Full);
                    }

                    boardManager.Move(game.Board1);
                    boardManager.Move(game.Board2);
                }
            }
        }

        private void StopGame(Game game, bool board1Full, bool board2Full)
        {
            if (game.State == GameState.Playing)
            {
                game.State = GameState.Stopped;
                game.Timer.AutoReset = false;
                game.Timer.Stop();
                game.Winner = GetWinner(board1Full, board2Full);

                gameEvents.gameEnded(game.Board1.Player, game.Winner == GameWinner.Player1);
                gameEvents.gameEnded(game.Board2.Player, game.Winner == GameWinner.Player2);

                CalculateAndSaveScore(game);
            }
        }

        private GameWinner GetWinner(bool board1Full, bool board2Full)
        {
            if (board1Full)
            {
                return GameWinner.Player2;
            }
            if (board2Full)
            {
                return GameWinner.Player1;
            }
            return GameWinner.None;
        }

        private void CalculateAndSaveScore(Game game)
        {
            if (!GeneralSettings.UseDatabase)
            {
                return;
            }

            var log = LogGame(game);

            PlayerRating player1Rating = GetOrCreateRating(game.Board1.Player.NickName);
            PlayerRating player2Rating = GetOrCreateRating(game.Board2.Player.NickName);

            var player1OriginalRating = player1Rating.Rating;

            player1Rating.Rating =
                ratingCalculator.NewRating(player1Rating.Rating, player2Rating.Rating, log.Player1Result);

            player2Rating.Rating =
                ratingCalculator.NewRating(player2Rating.Rating, player1OriginalRating, log.Player2Result);

            playerRatingRepo.Update(player1Rating);
            playerRatingRepo.Update(player2Rating);
        }

        private PlayerRating GetOrCreateRating(string playerNickName)
        {
            var result = playerRatingRepo.GetByPlayer(playerNickName);

            if (result == null)
            {
                PlayerRating rating = new PlayerRating()
                {
                    PlayerName = playerNickName,
                    Rating = ratingCalculator.GetInitialRating()
                };
                result = playerRatingRepo.Create(rating);
            }
            return result;
        }

        private GameLog LogGame(Game game)
        {
            GameLog log = new GameLog()
            {
                GameEnded = DateTime.Now,
                Player1 = game.Board1.Player.NickName,
                Player2 = game.Board2.Player.NickName,
                Player1Result = GetGameResult(game.Winner, 1),
                Player2Result = GetGameResult(game.Winner, 2),
            };

            gameLogRepo.Create(log);

            return log;
        }

        private double GetGameResult(GameWinner winner, int player)
        {
            if (winner == GameWinner.None)
            {
                return 0.5;
            }

            if (player == 1)
            {
                return (winner == GameWinner.Player1) ? 1 : 0;
            }
            if (player == 2)
            {
                return (winner == GameWinner.Player2) ? 1 : 0;
            }

            return 0;
        }

        #endregion
    }
}
