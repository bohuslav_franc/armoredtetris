﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackathonApp.Common.ObjectFactory;

namespace HackathonApp.Business.Impl
{
    [Component(ImplementationOf = typeof(IRatingCalculator))]
    public class RatingCalculator : IRatingCalculator
    {
        private const double KFactor = 32;
        private const double InitialScore = 1000;

        public double GetInitialRating()
        {
            return InitialScore;
        }

        public double NewRating(double currentRating, double opponentRating, double actualGameResult)
        {
            double exponent = (opponentRating - currentRating) / 400;
            double expectation = 1/(1+Math.Pow(10, exponent)  );

            return currentRating + KFactor * (actualGameResult - expectation);
        }
    }
}
