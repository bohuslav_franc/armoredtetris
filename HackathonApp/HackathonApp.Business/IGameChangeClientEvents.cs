﻿namespace HackathonApp.Business
{
    public interface IGameChangeClientEvents
    {
        void opponentConnected(Player player, Player opponent);

        void opponentIsReady(Player player);

        void matrixChanged(Player player, GameMatrix matrix);

        void opponentMatrixChanged(Player player, GameMatrix matrix);

        void componentMoved(Player player, FallingComponent component);

        void opponentComponentMoved(Player player, FallingComponent component);

        void setNextComponents(Player player, ComponentStructure[] component);

        void cooldownStarted(Player player, int duration);

        void cooldownExpired(Player player);

        void opponentCooldownStarted(Player player, int duration);

        void opponentCooldownExpired(Player player);

        void gameEnded(Player player, bool isWinner);

        void rowDestroyed(Player player);

        void junkCreated(Player player);
    }
}