﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using HackathonApp.Business.Impl;
using NUnit.Framework;

namespace HackathonApp.Business.Tests
{
    [TestFixture]
    public class ComponentStructureManipulationServiceTests
    {
        [Test]
        public void TestPredefinedStructures()
        {
            var service = new ComponentStructureManipulationService();
            service.PrepareCommonStructures();

            ComponentStructure structure;

            structure = service.GetStructure(ComponentType.I, 0);
            structure.Points[0].ShouldBe(-1, 0);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(1, 0);
            structure.Points[3].ShouldBe(2, 0);

            structure = service.GetStructure(ComponentType.I, 1);
            structure.Points[0].ShouldBe(0, -1);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(0, 1);
            structure.Points[3].ShouldBe(0, 2);

            structure = service.GetStructure(ComponentType.I, 2);
            structure.Points[0].ShouldBe(-1, 0);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(1, 0);
            structure.Points[3].ShouldBe(2, 0);

            structure = service.GetStructure(ComponentType.I, 3);
            structure.Points[0].ShouldBe(0, -1);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(0, 1);
            structure.Points[3].ShouldBe(0, 2);


            structure = service.GetStructure(ComponentType.J, 0);
            structure.Points[0].ShouldBe(-1, 0);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(1, 0);
            structure.Points[3].ShouldBe(1, 1);

            structure = service.GetStructure(ComponentType.J, 1);
            structure.Points[0].ShouldBe(0, -1);
            structure.Points[1].ShouldBe(0, 0);
            structure.Points[2].ShouldBe(0, 1);
            structure.Points[3].ShouldBe(-1, 1);

            structure = service.GetStructure(ComponentType.J, 2);
            structure.Points[0].ShouldBe(-1, -1);
            structure.Points[1].ShouldBe(-1, 0);
            structure.Points[2].ShouldBe(0, 0);
            structure.Points[3].ShouldBe(1, 0);

            structure = service.GetStructure(ComponentType.J, 3);
            structure.Points[0].ShouldBe(1, -1);
            structure.Points[1].ShouldBe(0, -1);
            structure.Points[2].ShouldBe(0, 0);
            structure.Points[3].ShouldBe(0, 1);


        }




    }

    public static class PointExtensions
    {
        public static void ShouldBe(this Point point, int x, int y)
        {
            point.X.Should().Be(x);
            point.Y.Should().Be(y);
        }
    }
}
