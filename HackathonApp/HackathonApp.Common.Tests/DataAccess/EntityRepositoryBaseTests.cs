﻿using FluentAssertions;
using HackathonApp.Common.DataAccess;
using NUnit.Framework;

namespace HackathonApp.Common.Tests.DataAccess
{
    [TestFixture]
    public class EntityRepositoryBaseTests
    {
        [Test]
        public void TestSaveForCreate()
        {
            var dc = new TestDataContext();

            bool created = false;
            bool updated = false;

            dc.OnCreate += (sender, o) => { created = true; };
            dc.OnUpdate += (sender, o) => { updated = true; };

            var repo = new EntityRepositoryBase<DataObjectBase>(dc);

            var obj = new DataObjectBase();

            repo.Save(obj);

            created.Should().BeTrue();
            updated.Should().BeFalse();
        }

        [Test]
        public void TestSaveForUpdate()
        {
            var dc = new TestDataContext();

            bool created = false;
            bool updated = false;

            dc.OnCreate += (sender, o) => { created = true; };
            dc.OnUpdate += (sender, o) => { updated = true; };

            var repo = new EntityRepositoryBase<DataObjectBase>(dc);

            var obj = new DataObjectBase
            {
                Id = 5
            };

            repo.Save(obj);

            created.Should().BeFalse();
            updated.Should().BeTrue();
        }

        [Test]
        public void TestGetById()
        {
            var dc = new TestDataContext();
            var repo = new EntityRepositoryBase<DataObjectBase>(dc);
            repo.Create(new DataObjectBase { Id = 1 });
            repo.Create(new DataObjectBase { Id = 2 });
            repo.Create(new DataObjectBase { Id = 3 });

            var result = repo.GetById(42);
            result.Should().BeNull();

            result = repo.GetById(2);
            result.Should().NotBeNull();
            result.Id.Should().Be(2);
        }

    }
}
