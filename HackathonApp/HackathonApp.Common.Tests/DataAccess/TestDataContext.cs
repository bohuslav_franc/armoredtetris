﻿using System;
using System.Collections.Generic;
using System.Linq;
using HackathonApp.Common.DataAccess;

namespace HackathonApp.Common.Tests.DataAccess
{
    public class TestDataContext : IDataContext
    {
        private readonly IDictionary<Type, IList<IPersistantObject>> data = new Dictionary<Type, IList<IPersistantObject>>();

        public event EventHandler<IPersistantObject> OnUpdate;
        public event EventHandler<IPersistantObject> OnCreate;
        public event EventHandler<IPersistantObject> OnDelete;
        public event EventHandler<Tuple<string, object[]>> OnCommand;


        public IQueryable<TEntity> Entity<TEntity>() where TEntity : class, IPersistantObject
        {
            return GetEntityData<TEntity>().Cast<TEntity>().AsQueryable();
        }

        public TEntity Update<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            OnUpdate?.Invoke(this, item);

            return item;
        }

        public TEntity Create<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            OnCreate?.Invoke(this, item);

            GetEntityData<TEntity>().Add(item);
            return item;
        }

        public void Delete<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            OnDelete?.Invoke(this, item);

            GetEntityData<TEntity>().Remove(item);
        }

        public int ExecuteCommand(string command, params object[] parameters)
        {
            OnCommand?.Invoke(this, new Tuple<string, object[]>(command, parameters));
            return 1;
        }

        private IList<IPersistantObject> GetEntityData<TEntity>() where TEntity : class, IPersistantObject
        {
            Type entityType = typeof(TEntity);

            if (!data.ContainsKey(entityType))
            {
                data[entityType] = new List<IPersistantObject>();
            }
            return data[entityType];
        }
    }
}
