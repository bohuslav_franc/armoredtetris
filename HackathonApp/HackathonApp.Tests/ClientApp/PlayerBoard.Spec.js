﻿(function () {
    'use strict';

    describe('PlayerBoard', function () {

        window.Audio = function () {
        }

        function createInstance() {
            var mediaRepo = new TankDuel.MediaRepository();
            var element = new jQuery('<div><div class="player-field"><canvas/></div><div class="player-board__name"></div></div>');
            var board = new TankDuel.PlayerBoard(mediaRepo, element, false);
            return board;
        }

        it('can be created', function () {
            var board = createInstance();
            expect(board).not.toBeNull();
            expect(board.canvas).not.toBeNull();
            expect(board.context).not.toBeNull();
            expect(board.coordinates).not.toBeNull();
            expect(board.nickNameElement.length).toBe(1);
        });

        it('can update', function () {
            var board = createInstance();
            board.update();
        });

        it('can set component', function () {
            var board = createInstance();
            board.setComponent({});
            expect(board.component).not.toBeNull();
            board.setComponent(null);
            expect(board.component).toBeNull();
        });

        it('can set matrix', function () {
            var board = createInstance();
            board.setMatrix({});
            expect(board.fixedPoints).not.toBeNull();
            board.setMatrix(null);
            expect(board.fixedPoints).toBeNull();
        });

        it('can set nickname', function () {
            var board = createInstance();
            board.setNickname("Karel");
            expect(board.nickname).toBe("Karel");
            expect(board.nickNameElement.text()).toBe("Karel");
        });
    });
})();