﻿using System;
using log4net;

namespace HackathonApp.Common.Log
{
    /// <summary>
    /// Basic implementation of <see cref="ILogger"/>
    /// </summary>
    public class LoggerBase : ILogger
    {
        private readonly ILog log;

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="logger"></param>
        public LoggerBase(string logger)
        {
            log = LogManager.GetLogger(logger);
        }

        /// <summary>
        /// logs debug level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        public void Debug(string message, params object[] parameters)
        {
            log.DebugFormat(message, parameters);
        }

        /// <summary>
        /// Logs info level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        public void Info(string message, params object[] parameters)
        {
            log.InfoFormat(message, parameters);
        }

        /// <summary>
        /// Logs warning level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        public void Warning(string message, params object[] parameters)
        {
            log.WarnFormat(message, parameters);
        }

        /// <summary>
        /// Logs warning level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        public void Warning(string message, Exception exception, params object[] parameters)
        {
            log.WarnFormat(message + Environment.NewLine + exception, parameters);
        }

        /// <summary>
        /// Logs error level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        public void Error(string message, params object[] parameters)
        {
            log.ErrorFormat(message, parameters);
        }

        /// <summary>
        /// Logs error level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        public void Error(string message, Exception exception, params object[] parameters)
        {
            log.ErrorFormat(message + Environment.NewLine + exception, parameters);
        }

        /// <summary>
        /// Logs fatal level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        public void Fatal(string message, params object[] parameters)
        {
            log.FatalFormat(message, parameters);
        }

        /// <summary>
        /// Logs fatal level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        public void Fatal(string message, Exception exception, params object[] parameters)
        {
            log.FatalFormat(message + Environment.NewLine + exception, parameters);
        }
    }
}
