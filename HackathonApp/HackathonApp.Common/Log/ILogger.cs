﻿using System;

namespace HackathonApp.Common.Log
{
    /// <summary>
    /// Application logger
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// logs debug level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        void Debug(string message, params object[] parameters);

        /// <summary>
        /// Logs info level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        void Info(string message, params object[] parameters);

        /// <summary>
        /// Logs warning level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        void Warning(string message, params object[] parameters);

        /// <summary>
        /// Logs warning level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        void Warning(string message, Exception exception, params object[] parameters);

        /// <summary>
        /// Logs error level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        void Error(string message, params object[] parameters);

        /// <summary>
        /// Logs error level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        void Error(string message, Exception exception, params object[] parameters);

        /// <summary>
        /// Logs fatal level message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        void Fatal(string message, params object[] parameters);

        /// <summary>
        /// Logs fatal level message with exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="parameters"></param>
        void Fatal(string message, Exception exception, params object[] parameters);

    }
}
