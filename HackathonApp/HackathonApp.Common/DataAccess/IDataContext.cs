using System.Linq;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Interface represents abstraction of EF DbContext
    /// </summary>
    public interface IDataContext
    {

        /// <summary>
        /// Returns queryable data set
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IQueryable<TEntity> Entity<TEntity>() where TEntity : class, IPersistantObject;

        /// <summary>
        /// Updates specified item in data store
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        TEntity Update<TEntity>(TEntity item) where TEntity : class, IPersistantObject;

        /// <summary>
        /// Insert new item into data store
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        TEntity Create<TEntity>(TEntity item) where TEntity : class, IPersistantObject;

        /// <summary>
        /// Delete item from data store
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        void Delete<TEntity>(TEntity item) where TEntity : class, IPersistantObject;

        /// <summary>
        /// Executes sql command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        int ExecuteCommand(string command, params object[] parameters);
    }
}