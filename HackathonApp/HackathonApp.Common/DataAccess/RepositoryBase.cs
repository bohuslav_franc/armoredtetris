﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Basic implementation of <see cref="IRepository{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RepositoryBase<T> : IRepository<T> where T : class, IPersistantObject
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="context"></param>
        public RepositoryBase(IDataContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Gets current instance of data context
        /// </summary>
        protected IDataContext Context { get; }

        /// <summary>
        /// Gets an instance of <see cref="IDbSet{TEntity}" /> for current entity type
        /// </summary>
        protected IQueryable<T> Entity
        {
            get { return Context.Entity<T>(); }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository{T}.Create"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual T Create(T obj)
        {
            try
            {
                var result = Context.Create(obj);
                return result;
            }
            catch (DbEntityValidationException exception)
            {
                throw new InvalidOperationException(BuildValidationErrorDetail(exception));
            }
        }

        /// <summary>
        /// Can be used to store generic object using data context
        /// </summary>
        /// <param name="obj"></param>
        protected void Create(IPersistantObject obj)
        {
            try
            {
                Context.Create(obj);
            }
            catch (DbEntityValidationException exception)
            {
                throw new InvalidOperationException(BuildValidationErrorDetail(exception));
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository{T}.Update"/>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public T Update(T item)
        {
            try
            {
                var result = Context.Update(item);
                return result;
            }
            catch (DbEntityValidationException exception)
            {
                throw new InvalidOperationException(BuildValidationErrorDetail(exception));
            }
        }

        /// <summary>
        /// Can be used to store generic object using data context
        /// </summary>
        /// <param name="item"></param>
        protected void Update(IPersistantObject item)
        {
            try
            {
                Context.Update(item);
            }
            catch (DbEntityValidationException exception)
            {
                throw new InvalidOperationException(BuildValidationErrorDetail(exception));
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository{T}.Delete"/>
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(T entity)
        {
            Context.Delete(entity);
        }

        /// <summary>
        ///     implementation of <see cref="IRepository{T}.GetAll" />
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            return Entity.ToList();
        }


        private string BuildValidationErrorDetail(DbEntityValidationException exception)
        {
            var text = new StringBuilder();

            foreach (var error in exception.EntityValidationErrors)
            {
                text.AppendLine(error.Entry.ToString());
                foreach (var validationError in error.ValidationErrors)
                {
                    text.AppendLine($"  - {validationError.PropertyName} - {validationError.ErrorMessage}");
                }
            }
            return text.ToString();
        }

        /// <summary>
        /// Returns data set for specified entity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        protected IQueryable<TEntity> GetEntity<TEntity>() where TEntity : class, IPersistantObject
        {
            return Context.Entity<TEntity>();
        }
    }
}