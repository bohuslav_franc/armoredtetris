﻿using System.Data.Entity;

namespace HackathonApp.Common.DataAccess
{
    internal class DevelopmentDatabaseInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            foreach (var creator in ObjectFactory.ObjectFactory.Current.ResolveAll<IInitialDataCreator>())
            {
                creator.InitializeData(context);
            }
        }
    }
}
