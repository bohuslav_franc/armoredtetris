﻿namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Base interface for persistant object with unique identifier
    /// </summary>
    public interface IDataObject : IPersistantObject 
    {
        /// <summary>
        /// Gets or sets the object primary key value
        /// </summary>
        long Id { get; set; }
    }

}
