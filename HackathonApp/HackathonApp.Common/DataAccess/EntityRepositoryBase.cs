﻿using System.Linq;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Basic implementation of <see cref="IEntityRepository{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityRepositoryBase<T> : RepositoryBase<T>, IEntityRepository<T> where T : class, IDataObject
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="context"></param>
        public EntityRepositoryBase(IDataContext context) : base(context)
        {
        }

        /// <summary>
        /// Implementation of <see cref="IEntityRepository{T}.GetById"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(long id)
        {
            return Entity.FirstOrDefault(e => e.Id == id);
        }

        /// <summary>
        /// Implementation of <see cref="IEntityRepository{T}.Save"/>
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Save(T entity)
        {
            if (entity.Id == default(long))
            {
                return Create(entity);
            }
            else
            {
                return Update(entity);
            }
        }
    }
}
