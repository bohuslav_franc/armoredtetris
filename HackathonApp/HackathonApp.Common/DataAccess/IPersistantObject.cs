﻿namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Marker interface for all objects which can be stored
    /// </summary>
    public interface IPersistantObject
    {
    }
}
