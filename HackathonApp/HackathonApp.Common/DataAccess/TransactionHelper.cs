﻿using System;
using System.Transactions;

namespace HackathonApp.Common.DataAccess
{

    /// <summary>
    /// Contains methods for working with <see cref="TransactionScope"/>
    /// </summary>
    public static class TransactionHelper
    {
        /// <summary>
        /// Begins a new transaction
        /// </summary>
        /// <returns></returns>
        public static TransactionScope BeginTransaction()
        {
            return BeginTransaction(IsolationLevel.ReadCommitted);
        }

        /// <summary>
        /// Begins a new transaction
        /// </summary>
        /// <param name="isolationLevel"></param>
        /// <returns></returns>
        public static TransactionScope BeginTransaction(IsolationLevel isolationLevel)
        {
            TransactionOptions options = new TransactionOptions
            {
                IsolationLevel = isolationLevel,
                Timeout = TimeSpan.FromMinutes(120)
            };

            return new TransactionScope(TransactionScopeOption.RequiresNew, options);
        }

        /// <summary>
        /// Returns new transaction scope, which suppreses currently opened transaction.
        /// Code inside using block with this transaction scope will run outside any transaction.
        /// </summary>
        /// <returns></returns>
        public static TransactionScope SuppressTransaction()
        {
            return new TransactionScope(TransactionScopeOption.Suppress);
        }

    }
}
