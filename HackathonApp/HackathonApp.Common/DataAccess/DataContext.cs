﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using HackathonApp.Common.ObjectFactory;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HackathonApp.Common.DataAccess
{
    public sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    }

    /// <summary>
    /// Main EF data context for entire application
    /// </summary>
    [Component(Lifetime = ComponentLifetime.PerRequest, ImplementationOf = typeof(IDataContext))]
    public class DataContext : IdentityDbContext<ApplicationUser>, IDataContext
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        public DataContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new DevelopmentDatabaseInitializer());

            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            Database.CommandTimeout = 120 * 60;
        }

        /// <summary>
        /// Implementation of <see cref="DbContext.OnModelCreating"/>
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            RegisterCommonEntities(modelBuilder);

            foreach (var module in ObjectFactory.ObjectFactory.Current.ResolveAll<IDataContextModule>())
            {
                module.OnModelCreating(modelBuilder);
            }
        }

        /// <summary>
        /// Registers common data model entities
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected void RegisterCommonEntities(DbModelBuilder modelBuilder)
        {
        }

        /// <summary>
        /// Returns instance of <see cref="IDbSet{TEntity}"/> for specified entity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public IQueryable<TEntity> Entity<TEntity>() where TEntity : class, IPersistantObject
        {
            return Set<TEntity>();
        }

        /// <summary>
        /// Implementation of <see cref="IDataContext.Update{TEntity}"/>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public TEntity Update<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            Entry(item).State = EntityState.Modified;
            SaveChanges();
            return item;

        }

        /// <summary>
        /// Implementation of <see cref="IDataContext.Create{TEntity}"/>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public TEntity Create<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            var saved = Set<TEntity>().Add(item);
            SaveChanges();
            return saved;
        }

        /// <summary>
        /// Implementation of <see cref="IDataContext.Delete{TEntity}"/>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="item"></param>
        public void Delete<TEntity>(TEntity item) where TEntity : class, IPersistantObject
        {
            Set<TEntity>().Remove(item);
            SaveChanges();
        }

        /// <summary>
        /// Implementation of <see cref="IDataContext.ExecuteCommand"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteCommand(string command, params object[] parameters)
        {
            return Database.ExecuteSqlCommand(command, parameters);
        }
    }
}

