﻿using System.ComponentModel.DataAnnotations;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Base class for all data entities
    /// </summary>
    public class DataObjectBase : IDataObject
    {
        /// <summary>
        /// Gets or sets obejct primary ID
        /// </summary>
        [Key]
        public long Id { get; set; }
    }
}
