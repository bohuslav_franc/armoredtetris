﻿namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Base interface for repository with read only capabilities
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntityRepository<T> : IRepository<T> where T : class, IDataObject
    {
        /// <summary>
        /// Returns object with specified ID value. Returns null when object with specified ID is not present in repository
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(long id);

        /// <summary>
        /// Creates or updates entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T Save(T entity);
    }
}