﻿using System.Collections.Generic;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Repository with CRUD capabilities
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class, IPersistantObject
    {

        /// <summary>
        /// Stores a new instance of item into repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        T Create(T item);

        /// <summary>
        /// Updates state in repository
        /// </summary>
        /// <param name="item"></param>
        T Update(T item);

        /// <summary>
        /// Deletes item from repository
        /// </summary>
        /// <param name="item"></param>
        void Delete(T item);

        /// <summary>
        /// Returns all items in data store
        /// </summary>
        /// <remarks>This will read all rows from database. Use only when neccessary.</remarks>
        /// <returns></returns>
        IEnumerable<T> GetAll();
    }
}
