﻿using System.Collections.Generic;

namespace HackathonApp.Common.DataAccess
{
    public interface IInitialDataCreator
    {
        void InitializeData(IDataContext context);
    }
}