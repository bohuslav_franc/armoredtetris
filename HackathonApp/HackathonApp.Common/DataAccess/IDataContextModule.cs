﻿using System.Data.Entity;

namespace HackathonApp.Common.DataAccess
{
    /// <summary>
    /// Interface representing module of data context.
    /// When main data context is creting, it will find all modules in loaded assemblies
    /// </summary>
    public interface IDataContextModule
    {
        /// <summary>
        /// When implemented, method can add entities into main data context
        /// </summary>
        /// <param name="modelBuilder"></param>
        void OnModelCreating(DbModelBuilder modelBuilder);
    }
}
