﻿using System;
using HackathonApp.Common.ObjectFactory.Impl;
using Microsoft.Practices.Unity;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Component registration record
    /// </summary>
    public class ComponentRegistration
    {
        /// <summary>
        /// Gets or sets the component name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the component interface type
        /// </summary>
        public Type Interface { get; set; }

        /// <summary>
        /// Gets or sets the component implementation type
        /// </summary>
        public Type Implementation { get; set; }

        /// <summary>
        /// Gets or sets the component lifetime manager type
        /// </summary>
        public ComponentLifetime ComponentLifetime { get; set; }

        /// <summary>
        /// Gets or sets value indicating if component uses AOP features
        /// </summary>
        public bool UseProxy { get; set; }

        internal LifetimeManager CreateLifetimeManager()
        {
            switch (ComponentLifetime)
            {
                case ComponentLifetime.Singleton: return new ContainerControlledLifetimeManager();
                case ComponentLifetime.PerRequest: return new RequestLifetimeManager();
                default: return new TransientLifetimeManager();
            }


        }

    }
}
