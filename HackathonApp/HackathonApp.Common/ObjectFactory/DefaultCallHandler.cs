﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using HackathonApp.Common.Log;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Implementation of <see cref="ICallHandler"/>
    /// </summary>
    public class DefaultCallHandler : ICallHandler
    {
        private readonly ILogger log = new LoggerBase(nameof(DefaultCallHandler));

        /// <summary>
        /// Implementation of <see cref="ICallHandler.Order"/>
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Implementation of <see cref="ICallHandler.Invoke"/>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="getNext"></param>
        /// <returns></returns>
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            try
            {
                MethodBase targetMethodInfo = FindMethod(input);

                LogMethodAttribute logAttr = FindAttribute<LogMethodAttribute>(targetMethodInfo);

                LogBefore(logAttr, input);

                IMethodReturn msg;
                try
                {
                    msg = getNext()(input, getNext);
                }
                catch (Exception exception)
                {
                    log.Error("Unhandled exception ocured in getNext() in call handler.", exception);
                    throw;
                }

                LogAfter(logAttr, input, msg);

                return msg;

            }
            catch (Exception exception)
            {
                log.Error("Unhandled exception ocured in call handler.", exception);
                throw;
            }
        }

        #region log methods

        /// <summary>
        /// Log method start
        /// </summary>
        /// <param name="attr"></param>
        /// <param name="input"></param>
        private void LogBefore(LogMethodAttribute attr, IMethodInvocation input)
        {
            // method log was not required
            if (attr == null)
            {
                return;
            }

            string methodName = String.Format(CultureInfo.InvariantCulture, "{0}.{1}", input.Target.GetType().FullName, input.MethodBase.Name);

            StringBuilder message = new StringBuilder();
            message.AppendFormat("Calling method {0}(", methodName);

            IList<string> arguments = new List<string>();
            foreach (var argument in input.Arguments)
            {
                if (argument == null)
                {
                    arguments.Add(attr.LogParameters ? "? null" : "?");
                    continue;
                }

                string typeName = argument.GetType().Name;

                if (attr.LogParameters)
                {
                    arguments.Add(String.Format(CultureInfo.InvariantCulture, "{1} {0}", argument, typeName));
                }
                else
                {
                    arguments.Add(typeName);
                }
            }
            message.Append(String.Join(", ", arguments));

            message.Append(")");

            log.Debug(message.ToString());
        }

        /// <summary>
        /// Log method finished
        /// </summary>
        /// <param name="attr"></param>
        /// <param name="input"></param>
        /// <param name="msg"></param>
        private void LogAfter(LogMethodAttribute attr, IMethodInvocation input, IMethodReturn msg)
        {
            if (attr == null)
            {
                return;
            }

            string methodName = String.Format(CultureInfo.InvariantCulture, "{0}.{1}", input.Target.GetType().FullName, input.MethodBase.Name);

            if (msg.Exception != null)
            {
                log.Error("Method {0} has finished with exception.", msg.Exception, methodName);
                return;
            }

            if (attr.LogResult)
            {
                log.Debug("Method {0} has finished with result: {1}", methodName, msg.ReturnValue);
            }
            else
            {
                log.Debug("Method {0} has finished.", methodName);
            }
        }

        #endregion

        #region support methods

        /// <summary>
        /// Returns first attribute of given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetMethod"></param>
        /// <returns></returns>
        private T FindAttribute<T>(MethodBase targetMethod) where T : Attribute
        {
            return targetMethod.GetCustomAttributes(typeof(T), true).FirstOrDefault() as T;
        }

        /// <summary>
        /// Returns targets method description
        /// </summary>
        /// <remarks>Methodbase on iunput is method of interface, it is neccessary to
        /// find method description of implementation.</remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        private MethodBase FindMethod(IMethodInvocation input)
        {
            MethodBase method = input.MethodBase;
            if (method.DeclaringType != null && method.DeclaringType.IsInterface)
            {
                Type[] paramTypes = method.GetParameters().Select(param => param.ParameterType).ToArray();

                method = input.Target.GetType().GetMethod(method.Name, paramTypes);
            }
            return method;
        }

        #endregion
    }
}