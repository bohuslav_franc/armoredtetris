﻿using System;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Attribute marks classes which should be registered in IOC as a component
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class ComponentAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the type of component interface
        /// </summary>
        public Type ImplementationOf { get; set; }

        /// <summary>
        /// Gets or sets the component name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets value indicating if component uses AOP proxy
        /// </summary>
        public bool UseProxy { get; set; }

        /// <summary>
        /// Gets or sets the component lifetime manager type
        /// </summary>
        public ComponentLifetime Lifetime { get; set; } 
    }
}
