using System;
using System.Collections.Generic;
using System.Web;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Static helper for maitaining request scope variables. When HttpContext is not available, it uses thred static context
    /// </summary>
    public static class RequestSingletonHelper
    {
        [ThreadStatic]
        private static IDictionary<string, object> threadValues;

        private static object GetThreadValue(string key)
        {
            InitValues();
            if (threadValues.ContainsKey(key))
            {
                return threadValues[key];
            }
            return null;
        }

        private static void SetThreadValue(string key, object value)
        {
            InitValues();
            threadValues[key] = value;
        }

        private static void InitValues()
        {
            if (threadValues == null)
            {
                threadValues = new Dictionary<string, object>();
            }
        }

        /// <summary>
        /// Returns value from scope
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetRequestValue(string key)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Items[key];
            }
            else
            {
                return GetThreadValue(key);
            }
        }

        /// <summary>
        /// Sets new value into scope
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetRequestValue(string key, object value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[key] = value;
            }
            else
            {
                SetThreadValue(key, value);
            }
        }


    }
}