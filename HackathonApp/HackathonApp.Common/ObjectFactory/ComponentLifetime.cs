﻿namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Type of IoC life time managers
    /// </summary>
    public enum ComponentLifetime
    {
        /// <summary>
        /// New instance is created each time component is resolved
        /// </summary>
        Transient,

        /// <summary>
        /// One component instance is created per web request or thred
        /// </summary>
        PerRequest,

        /// <summary>
        /// One component instance is created per entire app domain
        /// </summary>
        Singleton
    }
}