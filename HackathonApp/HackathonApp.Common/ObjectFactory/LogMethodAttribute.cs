﻿using System;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Attribute can be used to log method call from AOP
    /// </summary>
    public class LogMethodAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets value indicating if method result can be logged
        /// </summary>
        public bool LogResult { get; set; }

        /// <summary>
        /// Gets or sets value indicating if method parameters can be logged
        /// </summary>
        public bool LogParameters { get; set; }
    }
}