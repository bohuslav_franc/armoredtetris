﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HackathonApp.Common.ObjectFactory.Impl;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Class provides access to IoC container
    /// </summary>
    public class ObjectFactory
    {
        #region Singleton members
        private static readonly object lockObject = new object();
        private static ObjectFactory instance;

        /// <summary>
        /// Returns current instance of object container
        /// </summary>
        public static ObjectFactory Current => instance;

        /// <summary>
        /// Builds new instance of object factory
        /// </summary>
        /// <param name="container"></param>
        /// <param name="assemblies"></param>
        public static void BuildFactory(IUnityContainer container, IEnumerable<Assembly> assemblies)
        {
            if (instance != null)
            {
                return;
            }

            lock (lockObject)
            {
                if (instance != null)
                {
                    return;
                }

                container.AddNewExtension<Interception>();
                container.RegisterType<IMatchingRule, AnyMatchingRule>("AnyMatchingRule");
                container.RegisterType<ICallHandler, DefaultCallHandler>("MyCallHandler");
                PolicyDefinition policy = container.Configure<Interception>().AddPolicy("APXPolicy");
                policy.AddMatchingRule("AnyMatchingRule");
                policy.AddCallHandler("MyCallHandler");

                var factory = new ObjectFactory();
                foreach (var component in new ObjectFactoryBuilder().FindComponents(assemblies))
                {
                    RegisterComponent(container, component);
                }
                instance = factory;
                factory.container = container;
            }
        }

        private static void RegisterComponent(IUnityContainer container, ComponentRegistration component)
        {
            if (String.IsNullOrWhiteSpace(component.Name))
            {
                container.RegisterType(component.Interface, component.Implementation, component.CreateLifetimeManager());
            }
            else
            {
                container.RegisterType(component.Interface, component.Implementation, component.Name,
                    component.CreateLifetimeManager());
            }

            if (component.UseProxy)
            {
                container.Configure<Interception>().SetInterceptorFor(component.Interface, new TransparentProxyInterceptor());
            }
        }

        #endregion

        private IUnityContainer container;

        /// <summary>
        /// Returns instance of requested object from IoC container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Resolve<T>()
        {
            Type interfaceType = typeof(T);

            return (T)Resolve(interfaceType, null);
        }

        /// <summary>
        /// Returns all awailable instances of requested object type from IoC container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> ResolveAll<T>()
        {
            return container.ResolveAll<T>();
        }

        /// <summary>
        /// Returns instance of requested object based on its registration name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T Resolve<T>(string name)
        {
            Type interfaceType = typeof(T);

            return (T)Resolve(interfaceType, name);
        }

        /// <summary>
        /// Returns instance of requested object based on its registration name
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public object Resolve(Type type, string name)
        {
            return container.Resolve(type, name);
        }
    }
}
