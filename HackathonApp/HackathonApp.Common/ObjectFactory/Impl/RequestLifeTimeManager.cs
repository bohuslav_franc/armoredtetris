﻿using System;
using Microsoft.Practices.Unity;

namespace HackathonApp.Common.ObjectFactory.Impl
{
    /// <summary>
    /// Lifitime manager keaps unique instance per ASP.NET web request
    /// </summary>
    public class RequestLifetimeManager : LifetimeManager
    {
        private readonly string key = Guid.NewGuid().ToString();

        /// <summary>
        /// Implementation of <see cref="LifetimeManager.GetValue"/>
        /// </summary>
        /// <returns></returns>
        public override object GetValue()
        {
            return RequestSingletonHelper.GetRequestValue(key);
        }

        /// <summary>
        /// Implementation of <see cref="LifetimeManager.SetValue"/>
        /// </summary>
        /// <param name="value"></param>
        public override void SetValue(object value)
        {
            RequestSingletonHelper.SetRequestValue(key, value);
        }

        /// <summary>
        /// Implementation of <see cref="LifetimeManager.RemoveValue"/>
        /// </summary>
        public override void RemoveValue()
        {
            RequestSingletonHelper.SetRequestValue(key, null);
        }
    }
}
