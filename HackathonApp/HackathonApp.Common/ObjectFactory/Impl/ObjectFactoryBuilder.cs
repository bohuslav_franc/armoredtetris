﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HackathonApp.Common.ObjectFactory.Impl
{
    internal class ObjectFactoryBuilder
    {

        public IEnumerable<ComponentRegistration> FindComponents(IEnumerable<Assembly> assemblies)
        {
            foreach (var type in GetTypesToProcess(assemblies))
            {
                TypeInfo typeInfo = type.GetTypeInfo();
                foreach (ComponentAttribute component in typeInfo.GetCustomAttributes<ComponentAttribute>())
                {
                    yield return new ComponentRegistration
                    {
                        Name = component.Name,
                        Interface = component.ImplementationOf,
                        Implementation = type,
                        ComponentLifetime = component.Lifetime,
                        UseProxy = component.UseProxy
                    };
                }
            }
        }

        private IEnumerable<Type> GetTypesToProcess(IEnumerable<Assembly> assemblies)
        {
            foreach (var assm in assemblies)
            {
                foreach (Type type in assm.GetTypes())
                {
                    yield return type;
                }
            }
        }
    }
}