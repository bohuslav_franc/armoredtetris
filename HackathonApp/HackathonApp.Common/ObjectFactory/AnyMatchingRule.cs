﻿using System.Reflection;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace HackathonApp.Common.ObjectFactory
{
    /// <summary>
    /// Implementation of <see cref="IMatchingRule"/>
    /// </summary>
    public class AnyMatchingRule : IMatchingRule
    {
        /// <summary>
        /// Implementation of <see cref="IMatchingRule.Matches"/>
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool Matches(MethodBase member)
        {
            return true;
        }
    }
}