﻿using System.Configuration;

namespace HackathonApp.Common
{
    public static class GeneralSettings
    {

        public static bool UseDatabase
        {
            get
            {
                bool result;

                if (bool.TryParse(ConfigurationManager.AppSettings["useDb"], out result))
                {
                    return result;
                }
                return false;
            }
        }

        public static string ApplicationName
        {
            get
            {
                return ConfigurationManager.AppSettings["applicationName"];
            }
        }

    }
}
