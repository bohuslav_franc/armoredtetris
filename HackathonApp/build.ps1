<#
.SYNOPSIS
    Prepares invironment and build solution

.PARAMETER PauseBeforeExit
    Script will exit after some key is pressed.
#>

param (
    [switch]$PauseBeforeExit = $true
)



function getMsBuild() {
    $path="c:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\amd64\MSBuild.exe"  
    if (Test-Path $path) {
        return $path
    }
    $path= "registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MSBuild\14.0"
    if (Test-Path $path) {
        return (Get-ItemProperty $path).MSBuildOverrideTasksPath+"MSBuild.exe"
    }
    showError "MSBuild not found" 
}
$msbuild = getMsBuild

function showMessage {
    Write-Host -ForegroundColor Cyan $args[0]
}

function showError($message){
    Write-Host -ForegroundColor Red $message
    if ($PauseBeforeExit) {
        pause
    }
    exit
}

showMessage("Environment preparation ...");
. npm install
. tools\nuget.exe Restore HackathonApp.sln 
showMessage("Building ...");
. $msbuild HackathonApp.sln /t:Rebuild /property:Configuration=Debug /CLP:ErrorsOnly /m
showMessage("Building css ...");
. gulp less
New-Item -ItemType Directory -Force -Path HackathonApp\App_Data

if ($PauseBeforeExit) {
    pause
}
