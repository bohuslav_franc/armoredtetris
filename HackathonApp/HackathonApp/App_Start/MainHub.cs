﻿using System;
using System.Threading.Tasks;
using HackathonApp.Business;
using HackathonApp.Common;
using HackathonApp.Common.Log;
using Microsoft.AspNet.SignalR;

namespace Test1.App_Start
{
    public class MainHub : Hub
    {
        private readonly IGameManager gameManager;
        private readonly ILogger log = new LoggerBase(nameof(MainHub));

        public MainHub(IGameManager gameManager)
        {
            this.gameManager = gameManager;
        }

        public override Task OnConnected()
        {
            try
            {
                gameManager.UserConnected(Context.ConnectionId, GetCurrentUser());
                return base.OnConnected();
            }
            catch (Exception ex)
            {
                log.Error("Main hub failed.", ex);
                throw;
            }
        }

        private string GetCurrentUser()
        {
            if (GeneralSettings.UseDatabase)
            {
                return Context.User.Identity.Name;
            }
            return "unknown";
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            gameManager.UserDisconnected(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void playerReady()
        {
            gameManager.UserIsReady(Context.ConnectionId);
        }

        public void keyPressed(string keyId)
        {
            gameManager.KeyPressed(Context.ConnectionId, keyId);
        }

    }
}