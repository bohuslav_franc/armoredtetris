﻿using HackathonApp.Business;
using HackathonApp.Common.ObjectFactory;
using Microsoft.AspNet.SignalR;
using Test1.App_Start;

namespace HackathonApp.App_Start
{
    [Component(ImplementationOf = typeof(IGameChangeClientEvents))]
    public class GameChangeClientEvents : IGameChangeClientEvents
    {
        public void opponentConnected(Player player, Player opponent)
        {
            Client(player.ClientId).opponentConnected(opponent.NickName);
        }

        public void opponentIsReady(Player player)
        {
            Client(player.ClientId).opponentIsReady();
        }

        public void matrixChanged(Player player, GameMatrix matrix)
        {
            Client(player.ClientId).matrixChanged(matrix);
        }

        public void opponentMatrixChanged(Player player, GameMatrix matrix)
        {
            Client(player.ClientId).opponentMatrixChanged(matrix);
        }

        public void componentMoved(Player player, FallingComponent component)
        {
            Client(player.ClientId).componentMoved(component);
        }

        public void opponentComponentMoved(Player player, FallingComponent component)
        {
            Client(player.ClientId).opponentComponentMoved(component);
        }

        public void setNextComponents(Player player, ComponentStructure[] components)
        {
            Client(player.ClientId).setNextComponents(components);
        }

        public void cooldownStarted(Player player, int duration)
        {
            Client(player.ClientId).cooldownStarted(duration);
        }

        public void cooldownExpired(Player player)
        {
            Client(player.ClientId).cooldownExpired();
        }

        public void opponentCooldownStarted(Player player, int duration)
        {
            Client(player.ClientId).opponentCooldownStarted(duration);
        }

        public void opponentCooldownExpired(Player player)
        {
            Client(player.ClientId).opponentCooldownExpired();
        }

        public void gameEnded(Player player, bool isWinner)
        {
            Client(player.ClientId).gameEnded(isWinner);
        }

        public void rowDestroyed(Player player)
        {
            Client(player.ClientId).rowDestroyed();
        }

        public void junkCreated(Player player)
        {
            Client(player.ClientId).junkCreated();
        }


        private dynamic Client(string id)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<MainHub>();
            return context.Clients.Client(id);
        }
    }
}