﻿module TankDuel {

    export class WaitingBoard {
        private status: JQuery;
        private readyButton: JQuery;

        constructor(private element: JQuery, private iAmReadyEvent: () => void) {
            this.status = element.children('.waiting-overlay__status');
            this.readyButton = element.children('.waiting-overlay__ready-button');
            this.readyButton.on("click", () => this.iAmReady());
            if (GameSettings.Debug) {
                this.element.css('top', '80vh');
            }
        }

        public setStatus(status: string): void {
            this.status.text(status);
        }

        public showReadyButton(): void {
            this.readyButton.fadeIn('slow');
            if (GameSettings.Debug) {
                setTimeout(() => this.iAmReady(), 100);
            }
        }

        public hide(): void {
            this.element.fadeOut('slow');
        }

        private iAmReady() {
            this.readyButton.fadeOut('slow');
            this.iAmReadyEvent();
        }
    }
}