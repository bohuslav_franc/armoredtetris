﻿module TankDuel {

    /**
     * Provides twoway communication with server
     */
    export class Connection {

        /** SygnalR hub */
        private gameHub: any;

        private signalRIsStarted: boolean = false;

        constructor(
            private opponentConnectedEvent: IOpponentConnectedEvent,
            private opponentIsReadyEvent: IActionEvent,
            private componentMovedEvent: IComponentMovedEvent,
            private opponentComponentMovedEvent: IComponentMovedEvent,
            private nextComponentsEvent: INextComponentEvent,
            private matrixChangedEvent: IMatrixChangedEvent,
            private opponentMatrixChangedEvent: IMatrixChangedEvent,
            private cooldownStartedEvent: ICooldownStartedEvent,
            private cooldownExpiredEvent: IActionEvent,
            private opponentCooldownStartedEvent: ICooldownStartedEvent,
            private opponentCooldownExpiredEvent: IActionEvent,
            private gameEndedEvent: IGameEndedEvent,
            private junkCreatedEvent: IActionEvent,
            private rowDestroyedEvent: IActionEvent
        ) { }


        /**
         * Connects to server
         */
        public start() {
            this.startHub()
                .done(() => {
                    console.log("Connected");
                })
                .fail((a: any, b: any) => {
                    console.error(a);
                });

            this.gameHub = $.connection.mainHub;
            var client = this.gameHub.client;
            client.opponentConnected = (nickName) => this.opponentConnectedEvent(nickName);
            client.opponentIsReady = () => this.opponentIsReadyEvent();
            client.componentMoved = (fallingComponent: IFallingComponent) => this.componentMovedEvent(fallingComponent);
            client.opponentComponentMoved = (fallingComponent: IFallingComponent) => this.opponentComponentMovedEvent(fallingComponent);
            client.setNextComponents = (nextComponent: IComponentStructure[]) => this.nextComponentsEvent(nextComponent);
            client.matrixChanged = (matrix: IMatrix) => this.matrixChangedEvent(matrix);
            client.opponentMatrixChanged = (matrix: IMatrix) => this.opponentMatrixChangedEvent(matrix);
            client.cooldownStarted = (duration: number) => this.cooldownStartedEvent(duration);
            client.cooldownExpired = () => this.cooldownExpiredEvent();
            client.opponentCooldownStarted = (duration: number) => this.opponentCooldownStartedEvent(duration);
            client.opponentCooldownExpired = () => this.opponentCooldownExpiredEvent();
            client.gameEnded = (isWinner: boolean) => this.gameEndedEvent(isWinner);
            client.junkCreated = () => this.junkCreatedEvent();
            client.rowDestroyed = () => this.rowDestroyedEvent();
        }


        /**
         * Terminates connection
         */
        public stop() {
            $.connection.hub.stop();
        }


        /**
         * Sends indication that player is ready to play
         */
        public sendPlayerReady() {
            this.gameHub.server.playerReady();
        }


        /**
         * Send pressed key. Allowed keys (directions): U, D, L, R. 
         * @param keyCode Code of key
         */
        public sendKeyPressed(keyCode: string) {
            this.gameHub.server.keyPressed(keyCode);
        }


        private startHub(): JQueryPromise<any> {
            if (this.signalRIsStarted) {
                return $.when();
            }
            $.connection.hub.logging = true;
            var result = $.connection.hub.start();
            $.connection.hub.error(function (error) {
                console.log('SignalrAdapter: ' + error);
            });
            result.done(() => this.signalRIsStarted = true);
            return result;
        }

    }
}

