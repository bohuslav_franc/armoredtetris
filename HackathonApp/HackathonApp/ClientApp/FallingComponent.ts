﻿module TankDuel {

    /**
     * Renders actual falling componet  
     */
    export class FallingComponent {


        /**
         * Constructs component
         * @param context Canvas context
         * @param coordinates Coordinate system
         * @param medias Media repository
         * @param structure Strucure of falling component
         */
        constructor(
            private context: CanvasRenderingContext2D,
            private coordinates: Coordinates,
            private medias: MediaRepository,
            private structure: IComponentStructure) { }


        /**
         * Draw component to canvas
         * @param position Position of component
         */
        public draw(position: Position, offset: number = 0): void {
            var p = new Point();
            for (var pos of this.structure.Points) {
                let off = new Position(pos.X, pos.Y - offset + 1);
                p.draw(this.context, this.coordinates, this.medias, this.shift(position, off));
            }
        }


        /**
         * Shift position by relative offfset
         * @param position Basic position
         * @param offset Offset according to basic position
         */
        private shift(position: Position, offset: Position): Position {
            return new Position(position.X + offset.X, position.Y + offset.Y);
        }
    }
}