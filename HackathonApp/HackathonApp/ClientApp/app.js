var TankDuel;
(function (TankDuel) {
    /**
     * Renders waiting row for junk
     */
    var CooldownBoard = (function () {
        /**
         * Constructs board
         * @param mediaRepository Media repository
         * @param element jQuery with element of container for row
         */
        function CooldownBoard(mediaRepository, element, invertX) {
            this.mediaRepository = mediaRepository;
            this.element = element;
            this.invertX = invertX;
            this.canvas = element.children('canvas').get(0);
            this.context = this.canvas.getContext("2d");
            this.canvas.width = Math.ceil(element.innerWidth());
            this.canvas.height = Math.ceil(element.innerHeight());
            this.coordinates = new TankDuel.Coordinates(this.canvas.width, this.canvas.height, TankDuel.GameSettings.ColumnCount, 1, invertX);
        }
        /**
         * Draws row to canvas
         * @param duration Duration of row visibility
         */
        CooldownBoard.prototype.drawComponent = function (duration) {
            this.clear();
            var p = new TankDuel.Point();
            for (var i = 0; i < TankDuel.GameSettings.ColumnCount; i++) {
                p.draw(this.context, this.coordinates, this.mediaRepository, new TankDuel.Position(i, 0));
            }
            this.element.css('transition', 'margin ' + duration + 'ms linear');
            this.element.addClass('cooldown-row--move');
        };
        CooldownBoard.prototype.hideComponent = function () {
            this.clear();
            this.element.css('transition', 'margin 1ms linear');
            this.element.removeClass('cooldown-row--move');
        };
        /**
         * Clear canvas
         */
        CooldownBoard.prototype.clear = function () {
            this.canvas.width = this.canvas.width;
        };
        return CooldownBoard;
    }());
    TankDuel.CooldownBoard = CooldownBoard;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    /**
     * Coordinate system, transform element position to canvas pixel
     */
    var Coordinates = (function () {
        /**
         * Construct coordinate system
         * @param width Width of canvas in pixels
         * @param height Height of canvas in pixels
         * @param columns Columns count
         * @param rows Rows count
         * @param invertX Axis X is inverted (for opponent)
         */
        function Coordinates(width, height, columns, rows, invertX) {
            if (invertX === void 0) { invertX = false; }
            this.width = width;
            this.height = height;
            this.invertX = invertX;
            this.deltaX = width / rows;
            this.deltaY = height / columns;
        }
        Coordinates.prototype.toX = function (x) {
            if (this.invertX) {
                return x * this.deltaX;
            }
            else {
                return this.width - (x + 1) * this.deltaX;
            }
        };
        Coordinates.prototype.toY = function (y) {
            return this.height - (y + 1) * this.deltaY;
        };
        Coordinates.prototype.toWidth = function (w) {
            return w * this.deltaX;
        };
        Coordinates.prototype.toHeight = function (h) {
            return h * this.deltaY;
        };
        return Coordinates;
    }());
    TankDuel.Coordinates = Coordinates;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    /**
     * Renders actual falling componet
     */
    var FallingComponent = (function () {
        /**
         * Constructs component
         * @param context Canvas context
         * @param coordinates Coordinate system
         * @param medias Media repository
         * @param structure Strucure of falling component
         */
        function FallingComponent(context, coordinates, medias, structure) {
            this.context = context;
            this.coordinates = coordinates;
            this.medias = medias;
            this.structure = structure;
        }
        /**
         * Draw component to canvas
         * @param position Position of component
         */
        FallingComponent.prototype.draw = function (position, offset) {
            if (offset === void 0) { offset = 0; }
            var p = new TankDuel.Point();
            for (var _i = 0, _a = this.structure.Points; _i < _a.length; _i++) {
                var pos = _a[_i];
                var off = new TankDuel.Position(pos.X, pos.Y - offset + 1);
                p.draw(this.context, this.coordinates, this.medias, this.shift(position, off));
            }
        };
        /**
         * Shift position by relative offfset
         * @param position Basic position
         * @param offset Offset according to basic position
         */
        FallingComponent.prototype.shift = function (position, offset) {
            return new TankDuel.Position(position.X + offset.X, position.Y + offset.Y);
        };
        return FallingComponent;
    }());
    TankDuel.FallingComponent = FallingComponent;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    var MediaRepository = (function () {
        function MediaRepository() {
            this.point = this.createPoint();
            this.point2 = this.createPoint2();
            this.background = this.createBackground();
            this.shot = this.createShot();
            this.shotBigger = this.createShotBigger();
            this.moving = this.createMoving();
            this.stop = this.createStop();
            this.backgroundSound = this.createBackgroundSound();
            this.alarm = this.createAlarm();
        }
        MediaRepository.prototype.createPoint = function () {
            var image = new Image();
            image.src = "Images/point.png";
            return image;
        };
        MediaRepository.prototype.createPoint2 = function () {
            var image = new Image();
            image.src = "Images/point2.png";
            return image;
        };
        MediaRepository.prototype.createBackground = function () {
            var image = new Image();
            image.src = "Images/dirt.jpg";
            return image;
        };
        MediaRepository.prototype.createShot = function () {
            return new Audio("Sounds/shot.mp3");
        };
        MediaRepository.prototype.createShotBigger = function () {
            return new Audio("Sounds/shotBigger.mp3");
        };
        MediaRepository.prototype.createStop = function () {
            var sound = new Audio("Sounds/stop.mp3");
            sound.volume = 0.3;
            return sound;
        };
        MediaRepository.prototype.createMoving = function () {
            var sound = new Audio("Sounds/moving.mp3");
            sound.volume = 0.04;
            return sound;
        };
        MediaRepository.prototype.createBackgroundSound = function () {
            var sound = new Audio("Sounds/background.mp3");
            sound.loop = true;
            sound.volume = 0.02;
            return sound;
        };
        MediaRepository.prototype.createAlarm = function () {
            var sound = new Audio("Sounds/alarm.mp3");
            sound.loop = true;
            return sound;
        };
        MediaRepository.prototype.getPoint = function () {
            return this.point;
        };
        MediaRepository.prototype.getPoint2 = function () {
            return this.point2;
        };
        MediaRepository.prototype.getBackground = function () {
            return this.background;
        };
        MediaRepository.prototype.playShot = function () {
            this.replay(this.shot);
        };
        MediaRepository.prototype.playShotBigger = function () {
            this.replay(this.shotBigger);
        };
        MediaRepository.prototype.playMoving = function () {
            this.replay(this.moving);
        };
        MediaRepository.prototype.playStop = function () {
            this.replay(this.stop);
        };
        MediaRepository.prototype.playAlarm = function () {
            this.replay(this.alarm);
        };
        MediaRepository.prototype.stopAlarm = function () {
            this.alarm.pause();
            this.alarm.currentTime = 0;
        };
        MediaRepository.prototype.playBackground = function () {
            this.backgroundSound.play();
        };
        MediaRepository.prototype.replay = function (sound) {
            if (!sound.paused) {
                sound.pause();
                sound.currentTime = 0;
            }
            sound.play();
        };
        return MediaRepository;
    }());
    TankDuel.MediaRepository = MediaRepository;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    /**
     * Renders container with next component
     */
    var NextComponentBoard = (function () {
        /**
         * Construct board
         * @param mediaRepository Media repository
         * @param canvas Canvas for rendering
         */
        function NextComponentBoard(mediaRepository, canvas) {
            this.mediaRepository = mediaRepository;
            this.canvas = canvas;
            this.context = canvas.getContext("2d");
            var parent = $(canvas).parent();
            this.canvas.width = Math.ceil(parent.innerWidth());
            this.canvas.height = Math.ceil(parent.innerHeight());
            this.coordinates = new TankDuel.Coordinates(this.canvas.width, this.canvas.height, 4, 4);
        }
        /**
         * Draws component to canvas
         * @param componentStructure Strcuture of next component
         */
        NextComponentBoard.prototype.drawComponent = function (componentStructure) {
            // clear canvas
            this.canvas.width = this.canvas.width;
            var p = new TankDuel.Point();
            var component = new TankDuel.FallingComponent(this.context, this.coordinates, this.mediaRepository, componentStructure);
            component.draw(new TankDuel.Position(1, 1));
        };
        return NextComponentBoard;
    }());
    TankDuel.NextComponentBoard = NextComponentBoard;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    var PlayerBoard = (function () {
        function PlayerBoard(mediaRepository, mainElement, invertX) {
            this.mediaRepository = mediaRepository;
            this.mainElement = mainElement;
            this.invertX = invertX;
            this.fixedPoints = { Cells: [] };
            this.ready = false;
            this.componentOffset = 0;
            this.componentLastVerticalPosition = -1;
            this.canvas = mainElement.find('.player-field canvas')[0];
            var field = mainElement.find('.player-field');
            this.nickNameElement = mainElement.find('.player-board__name');
            this.context = this.canvas.getContext("2d");
            this.canvas.width = Math.ceil(field.innerWidth());
            this.canvas.height = Math.ceil(field.innerHeight());
            this.coordinates = new TankDuel.Coordinates(this.canvas.width, this.canvas.height, TankDuel.GameSettings.ColumnCount, TankDuel.GameSettings.RowCount, invertX);
        }
        PlayerBoard.prototype.update = function (delay) {
            this.clear();
            if (this.component) {
                this.component.draw(this.position, this.componentOffset);
            }
            if (this.fixedPoints && this.fixedPoints.Cells) {
                for (var x = 0; x < this.fixedPoints.Cells.length; x++) {
                    for (var y = 0; y < this.fixedPoints.Cells[x].length; y++) {
                        if (this.fixedPoints.Cells[x][y]) {
                            var p = new TankDuel.Point();
                            p.draw(this.context, this.coordinates, this.mediaRepository, new TankDuel.Position(x, y));
                        }
                    }
                }
            }
            /*for (let x = 3; x < 7; x++) {
                for (let y = 3; y < 7; y++) {
                    let p = new Point();
                    p.draw(this.context, this.coordinates, this.mediaRepository, new Position(x, y));
                }
            }
            this.context.strokeRect(0, 0, this.canvas.width, this.canvas.height);
            let component = new FallingComponent(this.context, this.coordinates, this.mediaRepository, { Points: [new Position(0, 0)] });
            component.draw(new Position(3, 3), this.componentOffset);
            */
            this.componentOffset += TankDuel.GameSettings.Delay / TankDuel.GameSettings.TickDelay;
        };
        PlayerBoard.prototype.setComponent = function (fallingComponent) {
            if (fallingComponent) {
                this.position = fallingComponent.Point;
                this.component = new TankDuel.FallingComponent(this.context, this.coordinates, this.mediaRepository, fallingComponent.Structure);
                if (this.componentLastVerticalPosition != this.position.Y) {
                    this.componentOffset = 0;
                }
                this.componentLastVerticalPosition = this.position.Y;
            }
            else {
                this.component = null;
                this.componentLastVerticalPosition = -1;
            }
        };
        PlayerBoard.prototype.setMatrix = function (matrix) {
            this.fixedPoints = matrix;
        };
        PlayerBoard.prototype.setNickname = function (nickname) {
            this.nickname = nickname;
            this.nickNameElement.text(nickname);
        };
        PlayerBoard.prototype.clear = function () {
            this.context.drawImage(this.mediaRepository.getBackground(), 0, 0, this.canvas.width, this.canvas.height);
        };
        return PlayerBoard;
    }());
    TankDuel.PlayerBoard = PlayerBoard;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    /**
     * Renders elementary unit to position
     */
    var Point = (function () {
        function Point() {
        }
        Point.prototype.draw = function (context, coordinates, medias, position) {
            if (coordinates.invertX) {
                context.drawImage(medias.getPoint2(), coordinates.toX(position.Y) + 1, coordinates.toY(position.X) + 1, coordinates.toWidth(1) - 1, coordinates.toHeight(1) - 1);
            }
            else {
                context.drawImage(medias.getPoint(), coordinates.toX(position.Y) + 1, coordinates.toY(position.X) + 1, coordinates.toWidth(1) - 1, coordinates.toHeight(1) - 1);
            }
        };
        return Point;
    }());
    TankDuel.Point = Point;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    var Position = (function () {
        function Position(X, Y) {
            this.X = X;
            this.Y = Y;
        }
        return Position;
    }());
    TankDuel.Position = Position;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    var WaitingBoard = (function () {
        function WaitingBoard(element, iAmReadyEvent) {
            var _this = this;
            this.element = element;
            this.iAmReadyEvent = iAmReadyEvent;
            this.status = element.children('.waiting-overlay__status');
            this.readyButton = element.children('.waiting-overlay__ready-button');
            this.readyButton.on("click", function () { return _this.iAmReady(); });
            if (TankDuel.GameSettings.Debug) {
                this.element.css('top', '80vh');
            }
        }
        WaitingBoard.prototype.setStatus = function (status) {
            this.status.text(status);
        };
        WaitingBoard.prototype.showReadyButton = function () {
            var _this = this;
            this.readyButton.fadeIn('slow');
            if (TankDuel.GameSettings.Debug) {
                setTimeout(function () { return _this.iAmReady(); }, 100);
            }
        };
        WaitingBoard.prototype.hide = function () {
            this.element.fadeOut('slow');
        };
        WaitingBoard.prototype.iAmReady = function () {
            this.readyButton.fadeOut('slow');
            this.iAmReadyEvent();
        };
        return WaitingBoard;
    }());
    TankDuel.WaitingBoard = WaitingBoard;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    /**
     * Provides twoway communication with server
     */
    var Connection = (function () {
        function Connection(opponentConnectedEvent, opponentIsReadyEvent, componentMovedEvent, opponentComponentMovedEvent, nextComponentsEvent, matrixChangedEvent, opponentMatrixChangedEvent, cooldownStartedEvent, cooldownExpiredEvent, opponentCooldownStartedEvent, opponentCooldownExpiredEvent, gameEndedEvent, junkCreatedEvent, rowDestroyedEvent) {
            this.opponentConnectedEvent = opponentConnectedEvent;
            this.opponentIsReadyEvent = opponentIsReadyEvent;
            this.componentMovedEvent = componentMovedEvent;
            this.opponentComponentMovedEvent = opponentComponentMovedEvent;
            this.nextComponentsEvent = nextComponentsEvent;
            this.matrixChangedEvent = matrixChangedEvent;
            this.opponentMatrixChangedEvent = opponentMatrixChangedEvent;
            this.cooldownStartedEvent = cooldownStartedEvent;
            this.cooldownExpiredEvent = cooldownExpiredEvent;
            this.opponentCooldownStartedEvent = opponentCooldownStartedEvent;
            this.opponentCooldownExpiredEvent = opponentCooldownExpiredEvent;
            this.gameEndedEvent = gameEndedEvent;
            this.junkCreatedEvent = junkCreatedEvent;
            this.rowDestroyedEvent = rowDestroyedEvent;
            this.signalRIsStarted = false;
        }
        /**
         * Connects to server
         */
        Connection.prototype.start = function () {
            var _this = this;
            this.startHub()
                .done(function () {
                console.log("Connected");
            })
                .fail(function (a, b) {
                console.error(a);
            });
            this.gameHub = $.connection.mainHub;
            var client = this.gameHub.client;
            client.opponentConnected = function (nickName) { return _this.opponentConnectedEvent(nickName); };
            client.opponentIsReady = function () { return _this.opponentIsReadyEvent(); };
            client.componentMoved = function (fallingComponent) { return _this.componentMovedEvent(fallingComponent); };
            client.opponentComponentMoved = function (fallingComponent) { return _this.opponentComponentMovedEvent(fallingComponent); };
            client.setNextComponents = function (nextComponent) { return _this.nextComponentsEvent(nextComponent); };
            client.matrixChanged = function (matrix) { return _this.matrixChangedEvent(matrix); };
            client.opponentMatrixChanged = function (matrix) { return _this.opponentMatrixChangedEvent(matrix); };
            client.cooldownStarted = function (duration) { return _this.cooldownStartedEvent(duration); };
            client.cooldownExpired = function () { return _this.cooldownExpiredEvent(); };
            client.opponentCooldownStarted = function (duration) { return _this.opponentCooldownStartedEvent(duration); };
            client.opponentCooldownExpired = function () { return _this.opponentCooldownExpiredEvent(); };
            client.gameEnded = function (isWinner) { return _this.gameEndedEvent(isWinner); };
            client.junkCreated = function () { return _this.junkCreatedEvent(); };
            client.rowDestroyed = function () { return _this.rowDestroyedEvent(); };
        };
        /**
         * Terminates connection
         */
        Connection.prototype.stop = function () {
            $.connection.hub.stop();
        };
        /**
         * Sends indication that player is ready to play
         */
        Connection.prototype.sendPlayerReady = function () {
            this.gameHub.server.playerReady();
        };
        /**
         * Send pressed key. Allowed keys (directions): U, D, L, R.
         * @param keyCode Code of key
         */
        Connection.prototype.sendKeyPressed = function (keyCode) {
            this.gameHub.server.keyPressed(keyCode);
        };
        Connection.prototype.startHub = function () {
            var _this = this;
            if (this.signalRIsStarted) {
                return $.when();
            }
            $.connection.hub.logging = true;
            var result = $.connection.hub.start();
            $.connection.hub.error(function (error) {
                console.log('SignalrAdapter: ' + error);
            });
            result.done(function () { return _this.signalRIsStarted = true; });
            return result;
        };
        return Connection;
    }());
    TankDuel.Connection = Connection;
})(TankDuel || (TankDuel = {}));
var TankDuel;
(function (TankDuel) {
    var GameSettings = (function () {
        function GameSettings() {
        }
        return GameSettings;
    }());
    GameSettings.RowCount = 22;
    GameSettings.ColumnCount = 11;
    GameSettings.Delay = 0.05;
    GameSettings.TickDelay = 0.5;
    GameSettings.Debug = false;
    TankDuel.GameSettings = GameSettings;
    var Game = (function () {
        function Game(boardElement, opponentBoardElement, nextComponent1Canvas, nextComponent2Canvas, cooldownRowElement, opponentCooldownRowElement, waitingElement, controlls) {
            var _this = this;
            this.boardElement = boardElement;
            this.opponentBoardElement = opponentBoardElement;
            this.nextComponent1Canvas = nextComponent1Canvas;
            this.nextComponent2Canvas = nextComponent2Canvas;
            this.cooldownRowElement = cooldownRowElement;
            this.opponentCooldownRowElement = opponentCooldownRowElement;
            this.waitingElement = waitingElement;
            this.resizeTimer = null;
            this.mediaRepository = new TankDuel.MediaRepository();
            this.board = new TankDuel.PlayerBoard(this.mediaRepository, boardElement, false);
            this.opponentBoard = new TankDuel.PlayerBoard(this.mediaRepository, opponentBoardElement, true);
            this.nextComponent1Board = new TankDuel.NextComponentBoard(this.mediaRepository, nextComponent1Canvas);
            this.nextComponent2Board = new TankDuel.NextComponentBoard(this.mediaRepository, nextComponent2Canvas);
            this.cooldownRow = new TankDuel.CooldownBoard(this.mediaRepository, cooldownRowElement, true);
            this.opponentCooldownRow = new TankDuel.CooldownBoard(this.mediaRepository, opponentCooldownRowElement, false);
            this.waitingBoard = new TankDuel.WaitingBoard(waitingElement, this.iAmReady.bind(this));
            this.connection = new TankDuel.Connection(this.opponentConnected.bind(this), this.opponentIsReady.bind(this), this.componentMoved.bind(this), this.opponentComponentMoved.bind(this), this.nextComponents.bind(this), this.matrixChanged.bind(this), this.opponentMatrixChanged.bind(this), this.cooldownStarted.bind(this), this.cooldownExpired.bind(this), this.opponentCooldownStarted.bind(this), this.opponentCooldownExpired.bind(this), this.gameEnded.bind(this), this.junkCreated.bind(this), this.rowDestroyed.bind(this));
            // desktop keyboard
            $(window).on("keydown", function (ev) { return _this.keyPressed(ev); });
            // mobile controlls
            controlls.find('[data-dir]').click(function (ev) { return _this.controllPressed(ev.currentTarget); });
            $(window).resize(function (ev) { return _this.resize(); });
            this.connection.start();
        }
        /**
         * Start game
         */
        Game.prototype.start = function () {
            var _this = this;
            var timer = setInterval(function () {
                try {
                    _this.board.update(GameSettings.Delay);
                    _this.opponentBoard.update(GameSettings.Delay);
                }
                catch (ex) {
                    console.error(ex);
                    clearInterval(timer);
                }
            }, GameSettings.Delay * 1000);
        };
        /**
         * Incoming event: opponent connected. Draw status.
         * @param nickname Login of opponent
         */
        Game.prototype.opponentConnected = function (nickname) {
            this.opponentBoard.setNickname(nickname);
            this.waitingBoard.setStatus(nickname + " connected");
            this.waitingBoard.showReadyButton();
        };
        /**
         * Outgoing event: send to server i am ready to play.
         */
        Game.prototype.iAmReady = function () {
            this.board.ready = true;
            this.connection.sendPlayerReady();
            if (this.opponentBoard.ready) {
                this.runGame();
            }
        };
        /**
         * Incoming event: opponent is ready. Draw status or play game.
         */
        Game.prototype.opponentIsReady = function () {
            this.opponentBoard.ready = true;
            if (!this.board.ready) {
                this.waitingBoard.setStatus(this.opponentBoard.nickname + " is ready");
            }
            else {
                this.runGame();
            }
        };
        /**
         * Players are ready, let's play.
         */
        Game.prototype.runGame = function () {
            this.waitingBoard.hide();
            this.mediaRepository.playBackground();
        };
        /**
         * Incoming event: my falling component moved.
         * @param fallingComponent
         */
        Game.prototype.componentMoved = function (fallingComponent) {
            if (!fallingComponent) {
                this.mediaRepository.playStop();
            }
            this.board.setComponent(fallingComponent);
        };
        /**
         * Incoming event: opponent's falling component moved.
         * @param fallingComponent
         */
        Game.prototype.opponentComponentMoved = function (fallingComponent) {
            this.opponentBoard.setComponent(fallingComponent);
        };
        /**
         * Incoming event: new list of future components
         * @param components
         */
        Game.prototype.nextComponents = function (components) {
            this.nextComponent1Board.drawComponent(components[0]);
            this.nextComponent2Board.drawComponent(components[1]);
        };
        /**
         * Incoming event: matrix of fixed points changes
         * @param matrix
         */
        Game.prototype.matrixChanged = function (matrix) {
            this.board.setMatrix(matrix);
        };
        /**
         * Incoming event: opponent's matrix of fixed points changes
         * @param matrix
         */
        Game.prototype.opponentMatrixChanged = function (matrix) {
            this.opponentBoard.setMatrix(matrix);
        };
        /**
         * Incoming event: new row is waiting
         * @param duration
         */
        Game.prototype.cooldownStarted = function (duration) {
            this.cooldownRow.drawComponent(duration);
            this.mediaRepository.playAlarm();
        };
        /**
         * Incoming event: new row dissapeared
         */
        Game.prototype.cooldownExpired = function () {
            this.cooldownRow.hideComponent();
            this.mediaRepository.stopAlarm();
        };
        /**
         * Incoming event: new row is waiting in opponent's board
         * @param duration
         */
        Game.prototype.opponentCooldownStarted = function (duration) {
            this.opponentCooldownRow.drawComponent(duration);
        };
        /**
         * Incoming event: new row for opponent dissapeared
         */
        Game.prototype.opponentCooldownExpired = function () {
            this.opponentCooldownRow.hideComponent();
        };
        /**
         * Incoming event: junk rows created
         */
        Game.prototype.junkCreated = function () {
            this.mediaRepository.playShotBigger();
        };
        /**
         * Incoming event: junk rows created in opponent' board
         */
        Game.prototype.rowDestroyed = function () {
            this.mediaRepository.playShot();
        };
        /**
         * Incoming event: game over. Redirect to antoher page
         * @param isWinner
         */
        Game.prototype.gameEnded = function (isWinner) {
            this.connection.stop();
            location.href = "Game/Over/?winner=" + (isWinner ? "true" : "false");
        };
        /**
         * User input event: controlls pressed on mobile view
         * @param isWinner
         */
        Game.prototype.controllPressed = function (target) {
            this.doMove($(target).attr('data-dir'));
        };
        /**
         * User input event: keyboard pressed
         * @param e
         */
        Game.prototype.keyPressed = function (e) {
            switch (e.keyCode) {
                case 37:
                    this.doMove("U");
                    break;
                case 38:
                    this.doMove("R");
                    break;
                case 39:
                    this.doMove("D");
                    break;
                case 40:
                    this.doMove("L");
                    break;
            }
            ;
        };
        /**
         * Outgoing event: send pressed key code
         * @param direction
         */
        Game.prototype.doMove = function (direction) {
            this.mediaRepository.playMoving();
            this.connection.sendKeyPressed(direction);
        };
        Game.prototype.resize = function () {
            var _this = this;
            clearTimeout(this.resizeTimer);
            this.resizeTimer = setTimeout(function () { return _this.resizeAsync(); }, 400);
        };
        Game.prototype.resizeAsync = function () {
            this.board = new TankDuel.PlayerBoard(this.mediaRepository, this.boardElement, false);
            this.opponentBoard = new TankDuel.PlayerBoard(this.mediaRepository, this.opponentBoardElement, true);
            this.cooldownRow = new TankDuel.CooldownBoard(this.mediaRepository, this.cooldownRowElement);
            this.opponentCooldownRow = new TankDuel.CooldownBoard(this.mediaRepository, this.opponentCooldownRowElement);
            this.nextComponent1Board = new TankDuel.NextComponentBoard(this.mediaRepository, this.nextComponent1Canvas);
            this.nextComponent2Board = new TankDuel.NextComponentBoard(this.mediaRepository, this.nextComponent2Canvas);
        };
        return Game;
    }());
    TankDuel.Game = Game;
})(TankDuel || (TankDuel = {}));
/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/signalr/signalr.d.ts" />
var game;
function startGame() {
    game = new TankDuel.Game($('.player-board--left'), $('.player-board--right'), $('.next-components-panel__next1 canvas')[0], $('.next-components-panel__next2 canvas')[0], $('.player-board--left .cooldown-row'), $('.player-board--right .cooldown-row'), $('.waiting-overlay'), $('.mobile-controlls'));
    try {
        game.start();
    }
    catch (ex) {
        console.log(ex);
    }
}
//# sourceMappingURL=app.js.map