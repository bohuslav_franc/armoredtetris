﻿module TankDuel {

    /**
     * Renders elementary unit to position
     */
    export class Point {
        public draw(context: CanvasRenderingContext2D, coordinates: Coordinates, medias: MediaRepository, position: Position): void {
            if (coordinates.invertX) {
                context.drawImage(
                    medias.getPoint2(),
                    coordinates.toX(position.Y) + 1,
                    coordinates.toY(position.X) + 1,
                    coordinates.toWidth(1) - 1,
                    coordinates.toHeight(1) - 1);
            } else {
                context.drawImage(
                    medias.getPoint(),
                    coordinates.toX(position.Y) + 1,
                    coordinates.toY(position.X) + 1,
                    coordinates.toWidth(1) - 1,
                    coordinates.toHeight(1) - 1);
            }
        }
    }
}