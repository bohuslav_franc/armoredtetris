﻿module TankDuel {

    export class MediaRepository {
        private point: HTMLImageElement;
        private point2: HTMLImageElement;
        private background: HTMLImageElement;
        private shot: HTMLAudioElement;
        private shotBigger: HTMLAudioElement;
        private moving: HTMLAudioElement;
        private stop: HTMLAudioElement;
        private backgroundSound: HTMLAudioElement;
        private alarm: HTMLAudioElement;

        constructor() {
            this.point = this.createPoint();
            this.point2 = this.createPoint2();
            this.background = this.createBackground();
            this.shot = this.createShot();
            this.shotBigger = this.createShotBigger();
            this.moving = this.createMoving();
            this.stop = this.createStop();
            this.backgroundSound = this.createBackgroundSound();
            this.alarm = this.createAlarm();
        }

        private createPoint(): HTMLImageElement {
            let image = new Image();
            image.src = "Images/point.png";
            return image;
        }

        private createPoint2(): HTMLImageElement {
            let image = new Image();
            image.src = "Images/point2.png";
            return image;
        }

        private createBackground(): HTMLImageElement {
            let image = new Image();
            image.src = "Images/dirt.jpg";
            return image;
        }

        private createShot(): HTMLAudioElement {
            return new Audio("Sounds/shot.mp3");
        }

        private createShotBigger(): HTMLAudioElement {
            return new Audio("Sounds/shotBigger.mp3");
        }

        private createStop(): HTMLAudioElement {
            let sound = new Audio("Sounds/stop.mp3");
            sound.volume = 0.3;
            return sound;
        }

        private createMoving(): HTMLAudioElement {
            let sound = new Audio("Sounds/moving.mp3");
            sound.volume = 0.04;
            return sound;
        }

        private createBackgroundSound(): HTMLAudioElement {
            let sound = new Audio("Sounds/background.mp3");
            sound.loop = true;
            sound.volume = 0.02;
            return sound;
        }

        private createAlarm(): HTMLAudioElement {
            let sound = new Audio("Sounds/alarm.mp3");
            sound.loop = true;
            return sound;
        }

        public getPoint(): HTMLImageElement {
            return this.point;
        }

        public getPoint2(): HTMLImageElement {
            return this.point2;
        }

        public getBackground(): HTMLImageElement {
            return this.background;
        }

        public playShot(): void {
            this.replay(this.shot);
        }

        public playShotBigger(): void {
            this.replay(this.shotBigger);
        }

        public playMoving(): void {
            this.replay(this.moving);
        }

        public playStop(): void {
            this.replay(this.stop);
        }

        public playAlarm(): void {
            this.replay(this.alarm);
        }

        public stopAlarm(): void {
            this.alarm.pause();
            this.alarm.currentTime = 0;
        }

        public playBackground(): void {
            this.backgroundSound.play();
        }

        private replay(sound: HTMLAudioElement) {
            if (!sound.paused) {
                sound.pause();
                sound.currentTime = 0;
            }
            sound.play();
        }
    }
}