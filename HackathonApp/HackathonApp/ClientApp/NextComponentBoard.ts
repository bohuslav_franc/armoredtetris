﻿module TankDuel {

    /**
     * Renders container with next component
     */
    export class NextComponentBoard {

        /** Context of canvas */
        private context: CanvasRenderingContext2D;

        /** Coordinate system */
        private coordinates: Coordinates;


        /**
         * Construct board
         * @param mediaRepository Media repository
         * @param canvas Canvas for rendering
         */
        public constructor(
            private mediaRepository: MediaRepository,
            private canvas: HTMLCanvasElement
        ) {
            this.context = canvas.getContext("2d");
            let parent = $(canvas).parent();
            this.canvas.width = Math.ceil(parent.innerWidth());
            this.canvas.height = Math.ceil(parent.innerHeight());
            this.coordinates = new Coordinates(this.canvas.width, this.canvas.height, 4, 4);
        }


        /**
         * Draws component to canvas
         * @param componentStructure Strcuture of next component
         */
        public drawComponent(componentStructure: IComponentStructure): void {
            // clear canvas
            this.canvas.width = this.canvas.width;
            var p = new Point();
            var component = new FallingComponent(this.context, this.coordinates, this.mediaRepository, componentStructure);
            component.draw(new Position(1, 1));
        }
    }
}