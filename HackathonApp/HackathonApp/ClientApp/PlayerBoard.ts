﻿module TankDuel {
    export class PlayerBoard {
        private canvas: HTMLCanvasElement;
        private context: CanvasRenderingContext2D;
        private nickNameElement: JQuery;
        private coordinates: Coordinates;
        private position: Position;
        private component: FallingComponent;
        private fixedPoints: IMatrix = { Cells: [] };
        public nickname: string;
        public ready: boolean = false;
        private componentOffset = 0;
        private componentLastVerticalPosition = -1;


        public constructor(
            private mediaRepository: MediaRepository,
            private mainElement: JQuery,
            private invertX: boolean
        ) {
            this.canvas = mainElement.find('.player-field canvas')[0] as HTMLCanvasElement;
            let field = mainElement.find('.player-field');
            this.nickNameElement = mainElement.find('.player-board__name');
            this.context = this.canvas.getContext("2d");
            this.canvas.width = Math.ceil(field.innerWidth());
            this.canvas.height = Math.ceil(field.innerHeight());
            this.coordinates = new Coordinates(this.canvas.width, this.canvas.height, GameSettings.ColumnCount, GameSettings.RowCount, invertX);
        }


        public update(delay: number) {
            this.clear();
            if (this.component) {
                this.component.draw(this.position, this.componentOffset);
            }
            if (this.fixedPoints && this.fixedPoints.Cells) {
                for (let x = 0; x < this.fixedPoints.Cells.length; x++) {
                    for (let y = 0; y < this.fixedPoints.Cells[x].length; y++) {
                        if (this.fixedPoints.Cells[x][y]) {
                            let p = new Point();
                            p.draw(this.context, this.coordinates, this.mediaRepository, new Position(x, y));
                        }
                    }
                }
            }
            /*for (let x = 3; x < 7; x++) {
                for (let y = 3; y < 7; y++) {
                    let p = new Point();
                    p.draw(this.context, this.coordinates, this.mediaRepository, new Position(x, y));
                }
            }
            this.context.strokeRect(0, 0, this.canvas.width, this.canvas.height);
            let component = new FallingComponent(this.context, this.coordinates, this.mediaRepository, { Points: [new Position(0, 0)] });
            component.draw(new Position(3, 3), this.componentOffset);
            */
            this.componentOffset += GameSettings.Delay / GameSettings.TickDelay;
        }


        public setComponent(fallingComponent: IFallingComponent) {
            if (fallingComponent) {
                this.position = fallingComponent.Point;
                this.component = new FallingComponent(this.context, this.coordinates, this.mediaRepository, fallingComponent.Structure);
                if (this.componentLastVerticalPosition != this.position.Y) {
                    this.componentOffset = 0;
                }
                this.componentLastVerticalPosition = this.position.Y;
            } else {
                this.component = null;
                this.componentLastVerticalPosition = -1;
            }
        }


        public setMatrix(matrix: IMatrix) {
            this.fixedPoints = matrix;
        }


        public setNickname(nickname: string) {
            this.nickname = nickname;
            this.nickNameElement.text(nickname);
        }


        private clear() {
            this.context.drawImage(this.mediaRepository.getBackground(), 0, 0, this.canvas.width, this.canvas.height);
        }
    }
}