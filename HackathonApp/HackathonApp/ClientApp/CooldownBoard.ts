﻿module TankDuel {

    /**
     * Renders waiting row for junk
     */
    export class CooldownBoard {
        /** Canvas for rendering */
        private canvas: HTMLCanvasElement;

        /** Context of canvas */
        private context: CanvasRenderingContext2D;

        /** Coordinate system */
        private coordinates: Coordinates;

        /**
         * Constructs board
         * @param mediaRepository Media repository
         * @param element jQuery with element of container for row
         */
        public constructor(
            private mediaRepository: MediaRepository,
            private element: JQuery,
            private invertX: boolean
        ) {
            this.canvas = element.children('canvas').get(0) as HTMLCanvasElement;
            this.context = this.canvas.getContext("2d");
            this.canvas.width = Math.ceil(element.innerWidth());
            this.canvas.height = Math.ceil(element.innerHeight());
            this.coordinates = new Coordinates(this.canvas.width, this.canvas.height, GameSettings.ColumnCount, 1, invertX);
        }


        /**
         * Draws row to canvas
         * @param duration Duration of row visibility
         */
        public drawComponent(duration: number) {
            this.clear();
            var p = new Point();
            for (let i = 0; i < GameSettings.ColumnCount; i++) {
                p.draw(this.context, this.coordinates, this.mediaRepository, new Position(i, 0));
            }
            this.element.css('transition', 'margin ' + duration + 'ms linear');
            this.element.addClass('cooldown-row--move');
        }

        public hideComponent() {
            this.clear();
            this.element.css('transition', 'margin 1ms linear');
            this.element.removeClass('cooldown-row--move');
        }

        /**
         * Clear canvas
         */
        private clear() {
            this.canvas.width = this.canvas.width;
        }
    }
}