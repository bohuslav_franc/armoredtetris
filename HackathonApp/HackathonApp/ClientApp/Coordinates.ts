﻿module TankDuel {

    /**
     * Coordinate system, transform element position to canvas pixel
     */
    export class Coordinates {
        /** Horizontal size of width of elementary unit */
        private deltaX: number;

        /** Vertical size of width of elementary unit */
        private deltaY: number;

        /**
         * Construct coordinate system
         * @param width Width of canvas in pixels
         * @param height Height of canvas in pixels
         * @param columns Columns count
         * @param rows Rows count
         * @param invertX Axis X is inverted (for opponent)
         */
        constructor(private width: number, private height: number, columns: number, rows: number, public invertX: boolean = false) {
            this.deltaX = width / rows;
            this.deltaY = height / columns;
        }

        public toX(x: number) {
            if (this.invertX) {
                return x * this.deltaX;
            } else {
                return this.width - (x + 1) * this.deltaX;
            }
        }

        public toY(y: number) {
            return this.height - (y + 1) * this.deltaY;
        }

        public toWidth(w: number) {
            return w * this.deltaX;
        }

        public toHeight(h: number) {
            return h * this.deltaY;
        }
    }
}