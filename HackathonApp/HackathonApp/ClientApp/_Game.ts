﻿module TankDuel {

    export class GameSettings {
        public static RowCount = 22;
        public static ColumnCount = 11;
        public static Delay = 0.05;
        public static TickDelay = 0.5;
        public static Debug = false;
    }

    export class Game {
        private mediaRepository: MediaRepository;
        private connection: Connection;

        private board: PlayerBoard;
        private opponentBoard: PlayerBoard;

        private nextComponent1Board: NextComponentBoard;
        private nextComponent2Board: NextComponentBoard;

        private cooldownRow: CooldownBoard;
        private opponentCooldownRow: CooldownBoard;

        private waitingBoard: WaitingBoard;


        public constructor(
            private boardElement: JQuery,
            private opponentBoardElement: JQuery,
            private nextComponent1Canvas: HTMLCanvasElement,
            private nextComponent2Canvas: HTMLCanvasElement,
            private cooldownRowElement: JQuery,
            private opponentCooldownRowElement: JQuery,
            private waitingElement: JQuery,
            controlls: JQuery
        ) {
            this.mediaRepository = new MediaRepository();
            this.board = new PlayerBoard(this.mediaRepository, boardElement, false);
            this.opponentBoard = new PlayerBoard(this.mediaRepository, opponentBoardElement, true);
            this.nextComponent1Board = new NextComponentBoard(this.mediaRepository, nextComponent1Canvas);
            this.nextComponent2Board = new NextComponentBoard(this.mediaRepository, nextComponent2Canvas);
            this.cooldownRow = new CooldownBoard(this.mediaRepository, cooldownRowElement, true);
            this.opponentCooldownRow = new CooldownBoard(this.mediaRepository, opponentCooldownRowElement, false);
            this.waitingBoard = new WaitingBoard(waitingElement, this.iAmReady.bind(this));
            this.connection = new Connection(
                this.opponentConnected.bind(this),
                this.opponentIsReady.bind(this),
                this.componentMoved.bind(this),
                this.opponentComponentMoved.bind(this),
                this.nextComponents.bind(this),
                this.matrixChanged.bind(this),
                this.opponentMatrixChanged.bind(this),
                this.cooldownStarted.bind(this),
                this.cooldownExpired.bind(this),
                this.opponentCooldownStarted.bind(this),
                this.opponentCooldownExpired.bind(this),
                this.gameEnded.bind(this),
                this.junkCreated.bind(this),
                this.rowDestroyed.bind(this)
            );
            // desktop keyboard
            $(window).on("keydown", (ev: JQueryEventObject) => this.keyPressed(ev));
            // mobile controlls
            controlls.find('[data-dir]').click((ev: JQueryEventObject) => this.controllPressed(ev.currentTarget));

            $(window).resize((ev: JQueryEventObject) => this.resize());
            this.connection.start();
        }


        /**
         * Start game
         */
        public start() {
            let timer = setInterval(() => {
                try {
                    this.board.update(GameSettings.Delay);
                    this.opponentBoard.update(GameSettings.Delay);
                } catch (ex) {
                    console.error(ex);
                    clearInterval(timer);
                }
            }, GameSettings.Delay * 1000);

        }


        /**
         * Incoming event: opponent connected. Draw status.
         * @param nickname Login of opponent
         */
        private opponentConnected(nickname) {
            this.opponentBoard.setNickname(nickname);
            this.waitingBoard.setStatus(nickname + " connected");
            this.waitingBoard.showReadyButton();
        }


        /**
         * Outgoing event: send to server i am ready to play.
         */
        private iAmReady() {
            this.board.ready = true;
            this.connection.sendPlayerReady();
            if (this.opponentBoard.ready) {
                this.runGame();
            }
        }


        /**
         * Incoming event: opponent is ready. Draw status or play game.
         */
        private opponentIsReady() {
            this.opponentBoard.ready = true;
            if (!this.board.ready) {
                this.waitingBoard.setStatus(this.opponentBoard.nickname + " is ready");
            } else {
                this.runGame();
            }
        }


        /**
         * Players are ready, let's play.
         */
        private runGame() {
            this.waitingBoard.hide();
            this.mediaRepository.playBackground();
        }


        /**
         * Incoming event: my falling component moved.
         * @param fallingComponent
         */
        private componentMoved(fallingComponent: IFallingComponent) {
            if (!fallingComponent) {
                this.mediaRepository.playStop();
            }
            this.board.setComponent(fallingComponent);
        }


        /**
         * Incoming event: opponent's falling component moved.
         * @param fallingComponent
         */
        private opponentComponentMoved(fallingComponent: IFallingComponent) {
            this.opponentBoard.setComponent(fallingComponent);
        }


        /**
         * Incoming event: new list of future components 
         * @param components
         */
        private nextComponents(components: IComponentStructure[]) {
            this.nextComponent1Board.drawComponent(components[0]);
            this.nextComponent2Board.drawComponent(components[1]);
        }


        /**
         * Incoming event: matrix of fixed points changes
         * @param matrix
         */
        private matrixChanged(matrix: IMatrix) {
            this.board.setMatrix(matrix);
        }


        /**
         * Incoming event: opponent's matrix of fixed points changes
         * @param matrix
         */
        private opponentMatrixChanged(matrix: IMatrix) {
            this.opponentBoard.setMatrix(matrix);

        }


        /**
         * Incoming event: new row is waiting
         * @param duration
         */
        private cooldownStarted(duration: number) {
            this.cooldownRow.drawComponent(duration);
            this.mediaRepository.playAlarm();
        }


        /**
         * Incoming event: new row dissapeared
         */
        private cooldownExpired() {
            this.cooldownRow.hideComponent();
            this.mediaRepository.stopAlarm();
        }

        /**
         * Incoming event: new row is waiting in opponent's board
         * @param duration
         */
        private opponentCooldownStarted(duration: number) {
            this.opponentCooldownRow.drawComponent(duration);
        }


        /**
         * Incoming event: new row for opponent dissapeared
         */
        private opponentCooldownExpired() {
            this.opponentCooldownRow.hideComponent();
        }


        /**
         * Incoming event: junk rows created
         */
        private junkCreated() {
            this.mediaRepository.playShotBigger();
        }


        /**
         * Incoming event: junk rows created in opponent' board
         */
        private rowDestroyed() {
            this.mediaRepository.playShot();
        }


        /**
         * Incoming event: game over. Redirect to antoher page
         * @param isWinner
         */
        private gameEnded(isWinner: boolean) {
            this.connection.stop();
            location.href = "Game/Over/?winner=" + (isWinner ? "true" : "false");
        }


        /**
         * User input event: controlls pressed on mobile view
         * @param isWinner
         */
        private controllPressed(target: any) {
            this.doMove($(target).attr('data-dir'));
        }


        /**
         * User input event: keyboard pressed
         * @param e
         */
        private keyPressed(e: JQueryEventObject) {
            switch (e.keyCode) {
                case 37: this.doMove("U"); break;
                case 38: this.doMove("R"); break;
                case 39: this.doMove("D"); break;
                case 40: this.doMove("L"); break;
            };
        }

        /**
         * Outgoing event: send pressed key code
         * @param direction
         */
        private doMove(direction: string) {
            this.mediaRepository.playMoving();
            this.connection.sendKeyPressed(direction);
        }

        private resizeTimer: any = null;
        private resize() {
            clearTimeout(this.resizeTimer);
            this.resizeTimer = setTimeout(() => this.resizeAsync(), 400);
        }


        private resizeAsync() {
            this.board = new PlayerBoard(this.mediaRepository, this.boardElement, false);
            this.opponentBoard = new PlayerBoard(this.mediaRepository, this.opponentBoardElement, true);
            this.cooldownRow = new CooldownBoard(this.mediaRepository, this.cooldownRowElement, true);
            this.opponentCooldownRow = new CooldownBoard(this.mediaRepository, this.opponentCooldownRowElement, false);
            this.nextComponent1Board = new NextComponentBoard(this.mediaRepository, this.nextComponent1Canvas);
            this.nextComponent2Board = new NextComponentBoard(this.mediaRepository, this.nextComponent2Canvas);
        }

    }
}
