﻿module TankDuel {

    // DTO interfaces
    export interface IFallingComponent {
        Point: Position,
        Structure: IComponentStructure;
    }

    export interface IComponentStructure {
        Points: Position[];
    }

    export interface IMatrix {
        Cells: boolean[][];
    }

    // Comunication events
    export interface IActionEvent {
        (): void;
    }

    export interface IOpponentConnectedEvent {
        (opponentNickname: string): void;
    }

    export interface IComponentMovedEvent {
        (fallingComponent: IFallingComponent): void;
    }

    export interface INextComponentEvent {
        (nextComponent: IComponentStructure[]): void;
    }

    export interface IMatrixChangedEvent {
        (matrix: IMatrix): void;
    }

    export interface ICooldownStartedEvent {
        (duration: number): void;
    }

    export interface IGameEndedEvent {
        (isWinner: boolean): void
    }
}

/**
 * Interface of SignalR. Must be out of TankDuel namespace.
 */
interface SignalR {
    mainHub: any
}
