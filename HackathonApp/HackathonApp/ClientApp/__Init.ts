﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/signalr/signalr.d.ts" />

var game: TankDuel.Game;

function startGame() {
    game = new TankDuel.Game(
        $('.player-board--left'),
        $('.player-board--right'),
        $('.next-components-panel__next1 canvas')[0] as HTMLCanvasElement,
        $('.next-components-panel__next2 canvas')[0] as HTMLCanvasElement,
        $('.player-board--left .cooldown-row'),
        $('.player-board--right .cooldown-row'),
        $('.waiting-overlay'),
        $('.mobile-controlls')
    );
    try {
        game.start();
    } catch (ex) {
        console.log(ex);
    }
}
