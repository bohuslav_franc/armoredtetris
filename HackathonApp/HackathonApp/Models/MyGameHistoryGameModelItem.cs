﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HackathonApp.Models
{
    public class MyGameHistoryGameModelItem
    {
        public DateTime Time { get; set; }
        public string Oponent { get; set; }
        public string Result { get; set; }
        public string RowClass { get; set; }
    }
}