﻿using HackathonApp.Common;
using HackathonApp.Common.ObjectFactory;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using Test1.App_Start;

[assembly: OwinStartupAttribute(typeof(HackathonApp.Startup))]
namespace HackathonApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalHost.DependencyResolver.Register(typeof(MainHub),
                () =>
                {
                    return ObjectFactory.Current.Resolve<MainHub>();
                });

            var hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            app.MapSignalR(hubConfiguration);
            if (GeneralSettings.UseDatabase)
            {
                GlobalHost.HubPipeline.RequireAuthentication();
        }
    }
}
}
