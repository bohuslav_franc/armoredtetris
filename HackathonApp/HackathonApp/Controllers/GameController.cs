﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HackathonApp.Controllers
{
    [Authorize]
    public class GameController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Over(Boolean winner)
        {
            return View(winner);
        }
    }
}