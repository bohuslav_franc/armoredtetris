﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HackathonApp.Business;
using HackathonApp.Common;
using HackathonApp.Models;

namespace HackathonApp.Controllers
{
    public class GamesHistoryController : Controller
    {
        private readonly IGameLogRepository gameLogRepo;
        private readonly IPlayerRatingRepository playerRatingRepo;

        public GamesHistoryController(IGameLogRepository gameLogRepo, IPlayerRatingRepository playerRatingRepo)
        {
            this.gameLogRepo = gameLogRepo;
            this.playerRatingRepo = playerRatingRepo;
        }


        // GET: GamesHistory
        public ActionResult Index()
        {
            return View(playerRatingRepo.GetTopTen());
        }

        public ActionResult MyGames()
        {
            string userLogin = GeneralSettings.UseDatabase ? User.Identity.Name : "Player1";
            var model = gameLogRepo.GetMyHistory(userLogin).Select(i => TransformViewItem(i, userLogin));

            return View(model);
        }

        private MyGameHistoryGameModelItem TransformViewItem(GameLog log, string userLogin)
        {
            var result = new MyGameHistoryGameModelItem();
            result.Time = log.GameEnded;

            if (log.Player1 == userLogin)
            {
                result.Oponent = log.Player2;
                result.Result = GetResultString(log.Player1Result);
                result.RowClass = GetResultRowClass(log.Player1Result);
            }
            else
            {
                result.Oponent = log.Player1;
                result.Result = GetResultString(log.Player2Result);
                result.RowClass = GetResultRowClass(log.Player2Result);
            }

            return result;
        }

        private string GetResultString(double result)
        {
            if (result <= 0.1)
            {
                return "lost";
            }
            if (result >= 0.9)
            {
                return "win";
            }
            return "draw";
        }

        private string GetResultRowClass(double result)
        {
            if (result <= 0.1)
            {
                return "table-danger";
            }
            if (result >= 0.9)
            {
                return "table-success";
            }
            return "";
        }

    }
}